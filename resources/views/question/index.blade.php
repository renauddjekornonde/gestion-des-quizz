
@extends('baseAdmin')
@section('content')
<!DOCTYPE html>
<html>
<head>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
{{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js"></script> --}}
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('assets/assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">


</head>
<body>
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
       <!-- Widgets  -->
       <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-1">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            {{-- <span class="currency float-left mr-1">$</span> --}}
                            <span class="count">{{$etudiants->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Apprenants</p>
                    </div><!-- /.card-left -->
                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-users"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-6">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count float-left">{{$matieres->count()}}</span><br>
                            {{-- <span>%</span> --}}
                        </h3>
                        <p class="text-light mt-1 m-0">Matieres</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-pen"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-3">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count">{{$questions->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Tuto</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-help1"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-2">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count">{{$niveaux->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Niveaux</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-graph1"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->
    </div>
    <!-- /Widgets -->

    @if ($message= Session::get('success'))
    <div class="alert alert-info">
    {{$message}}
    </div>
    @endif

               <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Quizz</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newQuestion" style="float:right;">New</button>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Tableau de Quizz</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                             <tr>
                                            <th>Question</th>
                                            <th>Matiere</th>
                                            <th>Niveau</th>
                                            <th>Creer</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($questionMatieres as $question)
                                            <tr>
                                                <td><a href="#matiere{{$question->id}}" data-toggle="modal">{{$question->contenu}}</a></td>

                                                <td>{{$question->questionMatiere->contenu}}</td>

                                                <td>{{$question->questionNiveau->nom}}</td>

                                                <td>{{$question->created_at->format('d/m/y')}}</td>
                                                <td>
                                                    <h4 style="float: left;">

                                                    <a href="#modifierquestion{{$question->id}}" style="color: green; text-decoration: none; " data-toggle="modal" data-dismiss="modal"><ion-icon name="pencil-outline"></ion-icon></a>
                                                </h4>

                                                <h4 style="float: right;">
                                                    <form action="{{route('destroyQuestion', $question->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')

                                                        <button type="submit" style="color: red; border: none; background:none; cursor:pointer;"><ion-icon name="trash-outline"></ion-icon></button>
                                                    </form>
                                                </h4>
                                            </td>
                                            </tr>
                                        <!-- Fenêtre modale pour voir les reponses d'une question -->
                                            <div class="modal" tabindex="-1" role="dialog" id="matiere{{$question->id}}">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Reponse</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">

                                                              @foreach ($reponses as $reponse)

                                                                @if ($reponse->question_id==$question->id)
                                                                <div class="form-group" >
                                                                    <label>{{$reponse->contenu}} </label>
                                                                    <label style="margin-left: 10%; " >{{$reponse->status}} </label>

                                                                    <h3 style="float: right;">

                                                                        <a href="#modifierReponse{{$reponse->id}}" style="color: green; text-decoration: none; " data-toggle="modal" data-dismiss="modal"><ion-icon name="pencil-outline"></ion-icon></a>
                                                                    </h3>

                                                                    <h3 style="float: right; margin-right: 10%;">
                                                                        <form action="{{route('destroyReponse', $reponse->id)}}" method="POST">
                                                                            @csrf
                                                                            @method('DELETE')

                                                                            <button type="submit" style="color: red; border: none; background: white; cursor:pointer;"><ion-icon name="trash-outline"></ion-icon></button>
                                                                        </form>
                                                                    </h3>
                                                                </div>
                                                                @endif
                                                       @endforeach

                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#newReponse{{$question->id}}" data-dismiss="modal">Ajouter reponse</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <!-- Fenêtre modale pour modifier une reponse -->
                                            @foreach ($reponses as $reponse)
                                            <div class="modal" tabindex="-1" role="dialog" id="modifierReponse{{$reponse->id}}">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Modifier une reponse</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Formulaire pour ajouter un nouvelle matiere -->
                                                            <form method="POST" action="{{route('updateReponse', $reponse->id)}}">
                                                                @csrf
                                                                @method('PATCH')
                                                                <div class="form-group">
                                                                    <label for="reponse">Reponse </label>
                                                                    <input type="text" class="form-control" id="contenu"  name="contenu" value="{{$reponse->contenu}}" required>
                                                                    @error('contenu')
                                                                        <p class="text text-danger">{{$message}}</p>
                                                                    @enderror
                                                                </div>

                                                                <label for="niveau">Status</label>
                                                                <div class="standardSelect">

                                                                <select data-placeholder="Choose a niveau..." class="form-control @error('status') is-invalid @enderror" tabindex="1" name="status" id="status" required>
                                                                        @if ($reponse->statsus== "vraie")
                                                                            <option value="{{ $reponse->status }}" selected>vraie</option>
                                                                        @endif
                                                                        <option value="vraie">vraie</option>
                                                                        <option value="faux">faux</option>

                                                                </select>
                                                                @error('status')
                                                                <p class="text text-danger">{{$message}}</p>
                                                                @enderror
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                                                            </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach

                                        <!-- Fenêtre modale pour modofier une question -->
                                        <div class="modal" tabindex="-1" role="dialog" id="modifierquestion{{$question->id}}">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Modifier une question</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <!-- Formulaire pour ajouter un nouvelle matiere -->
                                                        <form method="POST" action="{{route('updateQuestion', $question->id)}}">
                                                            @csrf
                                                            @method('PATCH')
                                                            <div class="form-group">
                                                                <label for="question">Question </label>
                                                                <input type="text" class="form-control" id="contenu" name="contenu" value="{{$question->contenu}}" required>
                                                                @error('contenu')
                                                                    <p class="text text-danger">{{$message}}</p>
                                                                @enderror
                                                            </div>

                                                            <label for="niveau">Matiere</label>
                                                            <div class="standardSelect">

                                                            <select data-placeholder="Choose a niveau..." class="form-control @error('matiere') is-invalid @enderror" tabindex="1" name="matiere" id="matiere" required>
                                                                @foreach ($matieres as $matiere)
                                                                    @if ($question->matiere_id== $matiere->id)
                                                                        <option value="{{ $matiere->id }}" selected>{{ $matiere->contenu }}</option>
                                                                    @endif
                                                                    <option value="{{ $matiere->id }}">{{ $matiere->contenu }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('matiere')
                                                            <p class="text text-danger">{{$message}}</p>
                                                            @enderror
                                                        </div>

                                                            <label for="niveau">Niveau</label>
                                                            <div class="standardSelect">

                                                            <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                                                                @foreach ($niveaux as $niveau)
                                                                    @if ($question->niveau_id== $niveau->id)
                                                                        <option value="{{ $niveau->id }}" selected>{{ $niveau->nom }}</option>
                                                                    @endif
                                                                    <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('niveau')
                                                            <p class="text text-danger">{{$message}}</p>
                                                            @enderror
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                                                        </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    </div>
    <!-- .animated -->
</div>

<!-- Fenêtre modale pour ajouter une question -->
<div class="modal" tabindex="-1" role="dialog" id="newQuestion">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajouter une nouvelle question</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Formulaire pour ajouter un nouvel étudiant -->
                <form method="POST" action="{{route('storeQuestion')}}">
                    @csrf

                    <div class="form-group">
                        <label for="contenu">Question </label>
                        <input type="text" class="form-control" id="contenu" placeholder="Entrez la question" name="contenu" required>
                        @error('nom')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                    <input type="hidden"  id="user"  name="user" value="{{Auth::user()->id}}">

                    <label for="niveau">Niveau</label>
                    <div class="standardSelect">

                    <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                        @foreach ($niveaux as $niveau)
                            <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                        @endforeach
                    </select>
                    @error('niveau')
                      <p class="text text-danger">{{$message}}</p>
                    @enderror
                  </div>

                    <label for="matiere">Matiere</label>
                    <div class="standardSelect">

                    <select data-placeholder="Choose a niveau..." class="form-control @error('matiere') is-invalid @enderror" tabindex="1" name="matiere" id="matiere" required>
                        @foreach ($matieres as $matiere)
                            <option value="{{ $matiere->id }}">{{ $matiere->contenu }}</option>
                        @endforeach
                    </select>
                    @error('matiere')
                      <p class="text text-danger">{{$message}}</p>
                    @enderror
                  </div>

                   <hr>
              <td>Vos reponses<button style="float:right" type="button" name="add" id="add-btn" class="btn btn-success"><ion-icon name="duplicate-sharp"></ion-icon></button></td>

              <table class="table-responsive" id="dynamicAddRemove">
               <tr>

               <td>
              <div class="mb-3">
              <label for="reponse" class="form-label">Reponse</label>
                <input type="text" class="form-control" id="contenuReponse" placeholder="Entrez la reponse" name="contenuReponse[]" required>
              </div>
              </td>

              <td>
              <div class="mb-3">
               <label for="status" class="form-label" style="margin-left: 15px">Status</label>
                <select  style="margin-top: 33px; margin-left: 10px" id="status" name="status[]"  class="form-select">
                    <option value="vraie">Vraie</option>
                    <option value="faux">Faux</option>
                </select>
              </div>
              </td>

              </tr>
              </table><br>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
                </form>
                  <script type="text/javascript">
    var i = 0;
    $("#add-btn").click(function(){
    ++i;
    $("#dynamicAddRemove").append('<tr><td> <label for="reponse" class="form-label">Reponse</label><input type="text" class="form-control" id="contenuReponse" placeholder="Entrez la reponse" name="contenuReponse[]"  required></td><td><label for="status" class="form-label" style="margin-left: 15px">Status</label><select style="margin-top: 33px; margin-left: 10px" id="status" name="status[]" class="form-select">  <option value="vraie">Vraie</option><option value="faux">Faux</option></select></td><td><button style="margin-top: 30px;" type="button" class="btn btn-danger remove-tr"><ion-icon name="close-circle-sharp"></ion-icon></button></td></tr>');
    });
    $(document).on('click', '.remove-tr', function(){
    $(this).parents('tr').remove();
    });
    </script>

            </div>

        </div>
    </div>
</div>

@foreach ($questionMatieres as $question)
    <!-- Fenêtre modale pour ajouter une question -->
<div class="modal" tabindex="-1" role="dialog" id="newReponse{{$question->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajouter une reponse</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Formulaire pour ajouter un nouvel étudiant -->
                <form method="POST" action="{{route('storeReponse')}}">
                    @csrf


                    <input type="hidden"  id="contenu" name="id_question" value="{{$question->id}}">

              <td>Vos reponses<button style="float:right" type="button" name="add" id="add-btn2" class="btn btn-success"><ion-icon name="duplicate-sharp"></ion-icon></button></td>

              <table class="table-responsive" id="dynamicAddRemove2">
               <tr>

               <td>
              <div class="mb-3">
              <label for="reponse" class="form-label">Reponse</label>
                <input type="text" class="form-control" id="contenuReponse" placeholder="Entrez la reponse" name="contenuReponse[]" required>
              </div>
              </td>

              <td>
              <div class="mb-3">
               <label for="status" class="form-label" style="margin-left: 15px">Status</label>
                <select  style="margin-top: 33px; margin-left: 10px" id="status" name="status[]"  class="form-select">
                    <option value="vraie">Vraie</option>
                    <option value="faux">Faux</option>
                </select>
              </div>
              </td>

              </tr>
              </table><br>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
                </form>
                  <script type="text/javascript">
    var i = 0;
    // var questionId = {{ htmlspecialchars($question->id, ENT_QUOTES, 'UTF-8') }};
    // alert(questionId);
    $("#add-btn2").click(function(){
    ++i;
    $("#dynamicAddRemove2").append('<tr><td> <label for="reponse" class="form-label">Reponse</label><input type="text" class="form-control" id="contenuReponse" placeholder="Entrez la reponse" name="contenuReponse[]"  required></td><td><label for="status" class="form-label" style="margin-left: 15px">Status</label><select style="margin-top: 33px; margin-left: 10px" id="status" name="status[]" class="form-select">  <option value="vraie">Vraie</option><option value="faux">Faux</option></select></td><td><button style="margin-top: 30px;" type="button" class="btn btn-danger remove-tr"><ion-icon name="close-circle-sharp"></ion-icon></button></td></tr>');
    });
    $(document).on('click', '.remove-tr', function(){
    $(this).parents('tr').remove();
    });
    </script>

            </div>

        </div>
    </div>
</div>
@endforeach




{{-- <script src="https://kit.fontawesome.com/04fe12c13f.js" crossorigin="anonymous"></script> --}}
    </body>
</html>
@endsection




