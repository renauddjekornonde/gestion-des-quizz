    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-center">
        <div class="container">
          <a class="navbar-brand" href="{{route('accueil')}}"><img src="{{asset('accueil/logo.png')}}" style="height:300; width: 150px;"></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mx-auto">

             @guest
             <li class="nav-item">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 active" href="{{route('accueil')}}">Accueil</a>
              </li>
             @endguest

             @auth
             <li class="nav-item">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 active" href="{{route('accueil')}}">Accueil</a>
              </li>
             @endauth

              @guest
                  <li class="nav-item dropdown">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 dropdown-toggle active" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Niveaux</a>
                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="#" id="btn1">Debutant</a>

                    <a class="nav-link" href="#" id="btn2">Elementaire</a>

                    <a class="nav-link" href="#" id="btn3">Intermediaire</a>
                </div>
              </li>
              @endguest

              <li class="nav-item">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 active" href="{{route('service')}}">Services</a>
              </li>

              @guest
              <li class="nav-item">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 active" href="{{route('login')}}">Login</a>
              </li>
              @endguest
              @auth
              <li class="nav-item">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 active" href="{{route('mescours')}}">Mes cours</a>
              </li>

            </ul>
           

            <div class="user-area dropdown float-right" >
                <a href="#" class="dropdown-toggle active" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle" @if(Auth::user()->avatar=="user.png") src="{{asset('assets/images/user.png')}}" @else src="{{Storage::url(Auth::user()->avatar)}}" @endif alt="Avatar" >
                </a>
                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="{{route('edit_profil')}}"><i class="fa fa-user"></i>My Profile</a>

                    <a class="nav-link" href="{{route('ap_logout')}}"><i class="fa fa-power-off"></i>Logout</a>
                </div>
             </div>
             @endauth

          </div>
        </div>
      </nav>
