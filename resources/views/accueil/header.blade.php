 <!-- Header-->
 <header class="bg-dark py-5" style="background-image: url('accueil/quiz.avif');">
    <div class="container px-4 px-lg-5 my-5">
        <div class="text-center text-white">
            <h1 class="display-4 fw-bolder">E-LEARNING</h1>
            <p class="lead fw-normal text-white-50 mb-0">Bienvenue dans notre plateforme de quiz qui offre une expérience de jeu facile à utiliser et enrichissante pour tester vos connaissances dans de nombreux domaines.</p>
        </div>
    </div>
</header>
