@extends('baseAccueil')
@section('content')

@guest

<section class="section py-5" id="section1" style="overflow-y: auto; height: 400px;">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
        @php
            $texteAffiche1 = false;
            $nombre1= 0;
        @endphp
      @foreach ($tutos as $tuto)
    @if ($tuto->tutoNiveau->nom=="Debutant")
    <div class="col-md-4">
        <div class="card">

            <img class="card-img-top" style="width:30%; display: block; margin: 0 auto;" src="{{Storage::url($tuto->image)}}" alt="Card image cap">
            <div class="card-body">

                <form action="{{route('favori')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$tuto->id}}">
                    <button class="btn-submit" style="border: none; background: rgb(178, 184, 95); font-size: 16px; line-height: 1; float:right;">
                        <span class="star">&#9733;</span>
                      </button>
                </form>

                 
                <h4 id="language" class="card-title mb-3">{{$tuto->tutoMatiere->contenu}}</h4>
                <p class="clipped-text" title="{{$tuto->description}}">{{$tuto->description}}</p>
            </div>


            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                <div class="text-center"><a class="btn btn-outline-dark mt-auto" data-bs-toggle="modal" href="#{{$tuto->id}}"  style="float:left" id="btnCours">Cours</a></div>
                <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="{{route('quizzEtudiant', $tuto->matiere_id)}}" style="float:right" id="btnQuizz">Quizz</a></div>



                @php
                $texteAffiche1 = true;
                $nombre1=$nombre1+1;
                @endphp
            </div>
        </div>
    </div>

      <!-- Fenêtre modale pour regarder une video -->
        <div class="modal" tabindex="-1" role="dialog" id="{{$tuto->id}}">
            <div class="modal-dialog" role="document" style="max-width: 70%; width: auto; margin-top:0px">
                <!--  -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{$tuto->titre}}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Fermer">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" >
                        <div class="embed-responsive embed-responsive-16by9" >
                            <iframe class="embed-responsive-item" src="{{$tuto->contenu}}" width="100%" height="550" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen style=" display: block; margin: 0 auto;"></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif
    @endforeach
    @if(!$texteAffiche1 && $nombre1==0)
        <div class="">
            {{-- <div class="alert-danger"> --}}
                <p class="" style="display: inline;">Il n'existe pas une vidéo pour le niveau Débutant</p>
            {{-- </div> --}}
        </div>
    @endif

        </div>
    </div>
</section>


<section class="section" id="section2" style="overflow-y: auto; height: 400px;">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
        @php
            $texteAffiche2 = false;
            $nombre2=0;
        @endphp
      @foreach ($tutos as $tuto)
      @if ($tuto->tutoNiveau->nom=="Elementaire")
      <div class="col-md-4">
        <div class="card">
            <img class="card-img-top" style="width:30%; display: block; margin: 0 auto;" src="{{Storage::url($tuto->image)}}" alt="Card image cap">
            <div class="card-body">

                <form action="{{route('favori')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$tuto->id}}">
                    <button class="btn-submit" style="border: none; background: rgb(178, 184, 95); font-size: 16px; line-height: 1; float:right;">
                        <span class="star">&#9733;</span>
                      </button>
                </form>

                <h4 id="language" class="card-title mb-3">{{$tuto->tutoMatiere->contenu}}</h4>
                <p class="clipped-text" title="{{$tuto->description}}">{{$tuto->description}}</p>
            </div>

            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                <div class="text-center"><a class="btn btn-outline-dark mt-auto" data-bs-toggle="modal" href="#{{$tuto->id}}"  style="float:left" id="btnCours">Cours</a></div>
                <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="{{route('quizzEtudiant', $tuto->matiere_id)}}" style="float:right" id="btnQuizz">Quizz</a></div>
                @php
                    $nombre2= $nombre2 +1;
                    $texteAffiche2 = true;
                @endphp
            </div>
        </div>
    </div>


      <!-- Fenêtre modale pour regarder une video -->
        <div class="modal" tabindex="-1" role="dialog" id="{{$tuto->id}}">
            <div class="modal-dialog" role="document" style="max-width: 70%; width: auto; margin-top:0px">
                <!--  -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{$tuto->titre}}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Fermer">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" >
                        <div class="embed-responsive embed-responsive-16by9" >
                            <iframe class="embed-responsive-item" src="{{$tuto->contenu}}" width="100%" height="550" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen style=" display: block; margin: 0 auto;"></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif
    @endforeach

    @if(!$texteAffiche2 && $nombre2==0)
        <div class="">
            {{-- <div class="alert-danger"> --}}
                <p class="" style="display: inline;">Il n'existe pas une vidéo pour le niveau Elementaire</p>
            {{-- </div> --}}
        </div>
    @endif

        </div>
    </div>
</section>


<section class="section" id="section3" style="overflow-y: auto; height: 400px;">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
        @php
            $texteAffiche = false;
            $nombre=0;
        @endphp
      @foreach ($tutos as $tuto)
      @if ($tuto->tutoNiveau->nom=="Intermediaire")
      <div class="col-md-4">
        <div class="card">
            <img class="card-img-top" style="width:30%; display: block; margin: 0 auto;" src="{{Storage::url($tuto->image)}}" alt="Card image cap">
            <div class="card-body">

                <form action="{{route('favori')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$tuto->id}}">
                    <button class="btn-submit" style="border: none; background: rgb(178, 184, 95); font-size: 16px; line-height: 1; float:right;">
                        <span class="star">&#9733;</span>
                      </button>
                </form>

                <h4 id="language" class="card-title mb-3">{{$tuto->tutoMatiere->contenu}}</h4>
                <p class="clipped-text" title="{{$tuto->description}}">{{$tuto->description}}</p>
            </div>

            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                <div class="text-center"><a class="btn btn-outline-dark mt-auto" data-bs-toggle="modal" href="#{{$tuto->id}}"  style="float:left" id="btnCours">Cours</a></div>
                <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="{{route('quizzEtudiant', $tuto->matiere_id)}}" style="float:right" id="btnQuizz">Quizz</a></div>
            @php
                $nombre= $nombre +1;
                $texteAffiche = true;
            @endphp
            </div>
        </div>
    </div>

      <!-- Fenêtre modale pour regarder une video -->
        <div class="modal" tabindex="-1" role="dialog" id="{{$tuto->id}}">
            <div class="modal-dialog" role="document" style="max-width: 70%; width: auto; margin-top:0px">
                <!--  -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{$tuto->titre}}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Fermer">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" >
                        <div class="embed-responsive embed-responsive-16by9" >
                            <iframe class="embed-responsive-item" src="{{$tuto->contenu}}" width="100%" height="550" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen style=" display: block; margin: 0 auto;"></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif
    @endforeach

    @if(!$texteAffiche && $nombre==0)
    <div class="">
        {{-- <div class="alert-danger"> --}}
            <p class="" style="display: inline;">Il n'existe pas une vidéo pour le niveau Intermediaire</p>
        {{-- </div> --}}
    </div>

    @endif

        </div>
    </div>
</section>

@endguest

@auth
<section class="py-5" style="overflow-y: auto; height: 400px;">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
        @php
            $texteAffiche = false;
            $nombre=0;
        @endphp
    @foreach ($tutos as $tuto)
      @if ($tuto->niveau_id==Auth::user()->niveau_id)

      <div class="col-md-4">
        <div class="card">
            <img class="card-img-top" style="width:30%; display: block; margin: 0 auto;" src="{{Storage::url($tuto->image)}}" alt="Card image cap">
            <div class="card-body">

                <form action="{{route('favori')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$tuto->id}}">
                    <button class="btn-submit" style="border: none; background: rgb(178, 184, 95); font-size: 16px; line-height: 1; float:right;">
                        <span class="star">&#9733;</span>
                      </button>
                </form>

                <h4 id="language" class="card-title mb-3">{{$tuto->tutoMatiere->contenu}}</h4>
                <p class="clipped-text" title="{{$tuto->description}}">{{$tuto->description}}</p>
            </div>

            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                <div class="text-center"><a class="btn btn-outline-dark mt-auto" data-bs-toggle="modal" href="#{{$tuto->id}}"  style="float:left" id="btnCours">Cours</a></div>
                <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="{{route('quizzEtudiant', $tuto->matiere_id)}}" style="float:right" id="btnQuizz">Quizz</a></div>
                @php
                    $texteAffiche = true;
                    $nombre=$nombre + 1;
                @endphp
            </div>
        </div>
    </div>

      <!-- Fenêtre modale pour regarder une video -->
        <div class="modal" tabindex="-1" role="dialog" id="{{$tuto->id}}">
            <div class="modal-dialog" role="document" style="max-width: 70%; width: auto; margin-top:0px">
                <!--  -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{$tuto->titre}}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Fermer">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" >
                        <div class="embed-responsive embed-responsive-16by9" >
                            <iframe class="embed-responsive-item" src="{{$tuto->contenu}}" width="100%" height="550" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen style=" display: block; margin: 0 auto;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @endforeach

    @if(!$texteAffiche && $nombre==0)
    <div class="">
        {{-- <div class="alert-danger"> --}}
            <p class="" style="display: inline;">Il n'existe pas une vidéo pour votre niveau</p>
        {{-- </div> --}}
    </div>
@endif

        </div>
    </div>
</section>
@endauth

<script>
   


</script>

@endsection
