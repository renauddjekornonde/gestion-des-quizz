<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="{{asset('accueil/style.css')}}">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- <link rel="icon" href="/img/drapeau.jpg"> --}}
    <title>SEN QUIZZ</title>
</head>
<body>
  <div class="background-slider">
    <div class="image"></div>
    <div class="dashboard">
		<ul>
			<li><a href="{{route('login')}}">Login</a></li>
			<li><a href="{{route('register')}}">Register</a></li>
			<li><a href="#">About</a></li>
		</ul>
	</div>
    <div class="content">
        <h1> QUIZZ <span>ESITEC</span></h1>
        <p>Bienvenue sur le plateforme de quizz de Esitec. Nous vous accompagnerons dans votre processus d'apprentissage dans les différents domaines de l'informatique</p>
        <a href="{{route('register')}}"><button> Let's Start</button></a>
    </div>
  </div>
</body>
</html>
