        @extends('baseProfil')
        @section('content')
        <hr class="mt-0 mb-4">
<div class="row">
    <div class="col-xl-4">
        <!-- Profile picture card-->
        <div class="card mb-4 mb-xl-0">
            <div class="card-header">Photo du profil</div>
            <div class="card-body text-center">
                <!-- Profile picture image-->
                <img class="img-account-profile rounded-circle mb-2" src="http://bootdey.com/img/Content/avatar/avatar1.png" alt="">
                <!-- Profile picture help block-->
                <div class="small font-italic text-muted mb-4"> Format : JPG et PNG qui ne doivent pas dépasser 5 MB</div>
                <!-- Profile picture upload button-->
                <button class="btn btn-primary" type="file">Importer image</button>
            </div>
        </div>
    </div>
    <div class="col-xl-8">
        <!-- Account details card-->
        @if ($message= Session::get('sucess'))
        <div class="alert alert-info">
        {{$message}}
    </div>
        @endif
        <div class="card mb-4">
            <div class="card-header">Informations du compte</div>
            <div class="card-body">
                <form method="POST" action="{{route('update_edit_profil', Auth::User()->id)}}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                    <!-- Form Group (username)-->
                    <div class="mb-3">
                        <label class="small mb-1" for="email">E-mail</label>
                        <input class="form-control" id="email" type="text" placeholder="Entrer votre email" name="email" value="{{Auth::User()->id}}" required>
                    </div>
                    <!-- Form Row-->
                    <div class="row gx-3 mb-3">
                        <!-- Form Group (first name)-->
                        <div class="col-md-6">
                            <label class="small mb-1" for="password">Mot de passe</label>
                            <input class="form-control" id="password" type="password" placeholder="Entrer votre mot de passe" name="password" value="{{Auth::User()->id}}" required>
                        </div>
                        <!-- Form Group (confirm password)-->
                        <!-- <div class="col-md-6">
                            <label class="small mb-1" for="inputLastName">Confirmer Mot de Passe</label>
                            <input class="form-control" id="inputLastName" type="text" placeholder="Confirmez votre mot de passe">
                        </div> -->
                    </div>
                    <!-- Form Row        -->
                    </div>
                    <!-- Save changes button-->
                    <button class="btn btn-primary" type="submit">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
       
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; ESITEC 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
@endsection
