<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        {{-- <meta name="author" content="" /> --}}
        <title>E-LEARNING</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="{{asset('accueil/assets/e-learning-icon-.jpg')}}" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />

        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{asset('accueil/css/styles.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{asset('accueil/stylec.css')}}">
    </head>
    <body>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="{{route('accueil')}}"><img src="{{asset('accueil/logo.png')}}" style="height:300; width: 150px;"></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">

              <li class="nav-item">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 active" href="{{route('accueil')}}">Accueil</a>
              </li>

              {{-- <li class="nav-item dropdown">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 dropdown-toggle active" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Niveaux</a>
                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="#">Debutant</a>

                    <a class="nav-link" href="#">Elementaire</a>

                    <a class="nav-link" href="#">Intermediaire</a>
                </div>
              </li> --}}

              <li class="nav-item">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 active" href="{{route('service')}}">Services</a>
              </li>

              @guest
              <li class="nav-item">
                <a class="nav-link text-uppercase fw-bold me-5 rounded-3 active" href="{{route('login')}}">Login</a>
              </li>
              @endguest

            </ul>
            @auth

            <div class="user-area dropdown float-right" >
                <a href="#" class="dropdown-toggle active" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle" @if(Auth::user()->avatar=="user.png") src="{{asset('assets/images/user.png')}}" @else src="{{Storage::url(Auth::user()->avatar)}}" @endif alt="Avatar" >
                </a>
                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="#"><i class="fa fa-user"></i>My Profile</a>

                    <a class="nav-link" href="{{route('ap_logout')}}"><i class="fa fa-power-off"></i>Logout</a>
                </div>
             </div>
             @endauth

                </div>
            </div>
        </nav>
        <!-- Header-->
        @include('accueil.header')

<section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="section-title text-center mb-4 pb-2">
                        <br><br><br>
                        <h4 class="title mb-4">Nos Services</h4>
                        <p class="text-muted para-desc mx-auto mb-0">E-LEARNING la plateforme pour vos cours et quizz.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card service-wrapper rounded border-0 shadow p-4">
                        <div class="icon text-center text-custom h1 shadow rounded bg-white">
                            <span class="uim-svg" style=""><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em"><rect width="20" height="15" x="2" y="3" class="uim-tertiary" rx="3"></rect><path class="uim-primary" d="M16,21H8a.99992.99992,0,0,1-.832-1.55469l4-6a1.03785,1.03785,0,0,1,1.66406,0l4,6A.99992.99992,0,0,1,16,21Z"></path></svg></span>
                        </div>
                        <div class="content mt-4">
                            <h5 class="title">Cours en vidéo</h5>
                            <p class="text-muted mt-3 mb-0"> Nous proposons des cours en vidéo dans différentes matières, tels que la programmation en java, c++, etc. Les utilisateurs peuvent suivre les cours en ligne à leur propre rythme.</p>
                        </div>
                        <div class="big-icon h1 text-custom">
                            <span class="uim-svg" style=""><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em"><rect width="20" height="15" x="2" y="3" class="uim-tertiary" rx="3"></rect><path class="uim-primary" d="M16,21H8a.99992.99992,0,0,1-.832-1.55469l4-6a1.03785,1.03785,0,0,1,1.66406,0l4,6A.99992.99992,0,0,1,16,21Z"></path></svg></span>
                        </div>
                    </div>
                </div><!--end col-->


                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card service-wrapper rounded border-0 shadow p-4">
                        <div class="icon text-center text-custom h1 shadow rounded bg-white">
                            <span class="uim-svg" style=""><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em"><path class="uim-tertiary" d="M20 11a.99018.99018 0 0 1-.71-.29 1.16044 1.16044 0 0 1-.21-.33008.94107.94107 0 0 1 0-.75976A1.02883 1.02883 0 0 1 19.29 9.29a1.04667 1.04667 0 0 1 1.41992 0 1.14718 1.14718 0 0 1 .21.33008.94107.94107 0 0 1 0 .75976 1.16044 1.16044 0 0 1-.21.33008A.99349.99349 0 0 1 20 11zM19 6.5a1.0032 1.0032 0 0 1 1-1h0a1.0032 1.0032 0 0 1 1 1h0a1.0032 1.0032 0 0 1-1 1h0A1.0032 1.0032 0 0 1 19 6.5zM20 4a.98979.98979 0 0 1-.91992-1.37988A1.02883 1.02883 0 0 1 19.29 2.29a1.04669 1.04669 0 0 1 1.41992 0 1.02883 1.02883 0 0 1 .21.33008A.98919.98919 0 0 1 20.71 3.71a1.16044 1.16044 0 0 1-.33008.21A.9994.9994 0 0 1 20 4zM7.03027 6.24023a.99364.99364 0 0 1 .7295-1.21h0a.9907.9907 0 0 1 1.21.7295h0a.99891.99891 0 0 1-.7295 1.21h0A.96451.96451 0 0 1 8 7H8A.99122.99122 0 0 1 7.03027 6.24023zm4-1a.99364.99364 0 0 1 .7295-1.21h0a.9907.9907 0 0 1 1.21.7295h0a.99891.99891 0 0 1-.7295 1.21h0A.96451.96451 0 0 1 12 6h0A1.00294 1.00294 0 0 1 11.03027 5.24023zm4-1a.99816.99816 0 0 1 .7295-1.21h0a1.00272 1.00272 0 0 1 1.21.7295h0a.99891.99891 0 0 1-.7295 1.21h0A.96451.96451 0 0 1 16 5h0A.99122.99122 0 0 1 15.03027 4.24023zM4 8A.99042.99042 0 0 1 3 7a.83154.83154 0 0 1 .08008-.37988A1.02883 1.02883 0 0 1 3.29 6.29 1.04669 1.04669 0 0 1 4.71 6.29a1.02883 1.02883 0 0 1 .21.33008A.99013.99013 0 0 1 4 8zM4 11a.99018.99018 0 0 1-.71-.29 1.16044 1.16044 0 0 1-.21-.33008.94107.94107 0 0 1 0-.75976A1.14718 1.14718 0 0 1 3.29 9.29 1.04667 1.04667 0 0 1 4.71 9.29a1.14718 1.14718 0 0 1 .21.33008.94107.94107 0 0 1 0 .75976 1.16044 1.16044 0 0 1-.21.33008A.99349.99349 0 0 1 4 11zM15 10a1.0032 1.0032 0 0 1 1-1h0a1.0032 1.0032 0 0 1 1 1h0a1.0032 1.0032 0 0 1-1 1h0A1.0032 1.0032 0 0 1 15 10zm-4 0a1.0032 1.0032 0 0 1 1-1h0a1.0032 1.0032 0 0 1 1 1h0a1.0032 1.0032 0 0 1-1 1h0A1.0032 1.0032 0 0 1 11 10zM7 10A1.0032 1.0032 0 0 1 8 9H8a1.0032 1.0032 0 0 1 1 1H9a1.0032 1.0032 0 0 1-1 1H8A1.0032 1.0032 0 0 1 7 10z"></path><polygon class="uim-primary" points="20 14 20 21 4 17 4 14 20 14"></polygon><path class="uim-primary" d="M20,22a.97427.97427,0,0,1-.24219-.03027l-16-4A.99961.99961,0,0,1,3,17V14a.99943.99943,0,0,1,1-1H20a.99943.99943,0,0,1,1,1v7a1.0005,1.0005,0,0,1-1,1ZM5,16.21875l14,3.5V15H5Z"></path></svg></span>
                        </div>
                        <div class="content mt-4">
                            <h5 class="title">Quiz</h5>
                            <p class="text-muted mt-3 mb-0">Nous proposons des quiz interactifs pour tester les connaissances des utilisateurs après chaque cours ou module. Les quiz peuvent être conçus pour aider les utilisateurs à identifier les domaines dans lesquels ils ont besoin d'amélioration.</p>
                        </div>
                            </div>
                        </div><!--end col-->
                        <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                            <div class="card service-wrapper rounded border-0 shadow p-4">
                                <div class="icon text-center text-custom h1 shadow rounded bg-white">
                                    <span class="uim-svg" style=""><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em"><path class="uim-primary" d="M12,6a.99974.99974,0,0,1,1,1v4.42249l2.09766,1.2113a1.00016,1.00016,0,0,1-1,1.73242l-2.59766-1.5A1.00455,1.00455,0,0,1,11,12V7A.99974.99974,0,0,1,12,6Z"></path><path class="uim-tertiary" d="M2,12A10,10,0,1,0,12,2,10,10,0,0,0,2,12Zm9-5a1,1,0,0,1,2,0v4.42249l2.09766,1.2113a1.00016,1.00016,0,0,1-1,1.73242l-2.59766-1.5A1.00455,1.00455,0,0,1,11,12Z"></path></svg></span>
                                </div>
                                <div class="content mt-4">
                                    <h5 class="title">Service 24/7</h5>
                                    <p class="text-muted mt-3 mb-0">Nous proposons de nouvelles matières et de nouveaux quizz chaque jour.</p>
                                </div>
                                <div class="big-icon h1 text-custom">
                                    <span class="uim-svg" style=""><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em"><path class="uim-primary" d="M12,6a.99974.99974,0,0,1,1,1v4.42249l2.09766,1.2113a1.00016,1.00016,0,0,1-1,1.73242l-2.59766-1.5A1.00455,1.00455,0,0,1,11,12V7A.99974.99974,0,0,1,12,6Z"></path><path class="uim-tertiary" d="M2,12A10,10,0,1,0,12,2,10,10,0,0,0,2,12Zm9-5a1,1,0,0,1,2,0v4.42249l2.09766,1.2113a1.00016,1.00016,0,0,1-1,1.73242l-2.59766-1.5A1.00455,1.00455,0,0,1,11,12Z"></path></svg></span>
                                </div>
                            </div>
                        </div><!--end col-->

                        </div>
            </div><!--end row-->
        </div>
        </div>
        <br><br><br><br><br>
    </section>
    </body>

        <!-- Footer-->
        <footer  class="py-4 bg-dark fixed-bottom">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; ESITEC 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{asset('accueil/js/scripts.js')}}"></script>

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/assets/css/chat.min.css">
        <script>
            var botmanWidget = {
            aboutText: 'Ecrivez quelque chose',
            introMessage: "✋ Bonjour ! Je suis une assistante de la plateforme web E-LEARNING QUIZZ que puis-je faire pour vous?"
            };
        </script>

    <script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>
</html>


























