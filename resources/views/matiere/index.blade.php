@extends('baseAdmin')
@section('content')
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
       <!-- Widgets  -->
       <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-1">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            {{-- <span class="currency float-left mr-1">$</span> --}}
                            <span class="count">{{$etudiants->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Apprenants</p>
                    </div><!-- /.card-left -->
                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-users"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-6">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count float-left">{{$matieres->count()}}</span><br>
                            {{-- <span>%</span> --}}
                        </h3>
                        <p class="text-light mt-1 m-0">Matieres</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-pen"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-3">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count">{{$questions->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Tuto</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-help1"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-2">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count">{{$niveaux->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Niveaux</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-graph1"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->
    </div>
    <!-- /Widgets -->
    @if ($message= Session::get('success'))
    <div class="alert alert-info">
    {{$message}}
    </div>
    @endif

        <div class="clearfix"></div>
        <!-- Orders -->
          <div class="col-lg-10">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Matieres</strong>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newMatiere" style="float:right;">New</button>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                      <th scope="col">#</th>
                                      <th scope="col">Nom</th>
                                      <th scope="col">Niveau</th>
                                      <th scope="col">Date Création</th>
                                      <th scope="col">View</th>
                                  </tr>
                              </thead>
                              <tbody>
                               @foreach($matieres as $matiere)
                                     <tr>
                                    <th scope="row">{{$matiere->id}}</th>
                                    <td>{{$matiere->contenu}}</td>
                                    <td>{{$matiere->matiereNiveau->nom}}</td>
                                    <td>{{$matiere->created_at->format('d/m/y')}}</td>
                                    <td>
                                        <h3  style="float: left;">

                                            <a href="#modifierMatiere{{$matiere->id}}" style="color: green; text-decoration: none; ;" data-toggle="modal"><ion-icon name="pencil-outline"></ion-icon></a></h3>

                                            <h3 style="float: right;">
                                                <form action="{{route('destroyMatiere', $matiere->id)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')

                                                    <button type="submit" style="color: red; border: none; background: white; cursor:pointer;"><ion-icon name="trash-outline"></ion-icon></button>
                                                </form>
                                            </h3>
                                    </td>
                                    <!-- Fenêtre modale pour modofier une matiere -->
                                    <div class="modal" tabindex="-1" role="dialog" id="modifierMatiere{{$matiere->id}}">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Modifier une matiere</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Formulaire pour ajouter un nouvelle matiere -->
                                                    <form method="POST" action="{{route('updateMatiere', $matiere->id)}}">
                                                        @csrf
                                                        @method('PATCH')
                                                        <div class="form-group">
                                                            <label for="nom">Nom </label>
                                                            <input type="text" class="form-control" id="nom" placeholder="Entrez le nom de le complet" name="nom" value="{{$matiere->contenu}}" required>
                                                            @error('nom')
                                                                <p class="text text-danger">{{$message}}</p>
                                                            @enderror
                                                        </div>

                                                        <label for="niveau">Niveau</label>
                                                        <div class="standardSelect">

                                                        <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                                                            @foreach ($niveaux as $niveau)
                                                                @if ($matiere->niveau_id== $niveau->id)
                                                                    <option value="{{ $niveau->id }}" selected>{{ $niveau->nom }}</option>
                                                                 @endif
                                                                <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('niveau')
                                                        <p class="text text-danger">{{$message}}</p>
                                                        @enderror
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                                                    </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </tr>
                               @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        <!-- /.orders -->

    </div>
    <!-- .animated -->
</div>

<!-- Fenêtre modale pour ajouter une nouvelle matiere -->
<div class="modal" tabindex="-1" role="dialog" id="newMatiere">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajouter une nouvelle matiere</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Formulaire pour ajouter un nouvelle matiere -->
                <form method="POST" action="{{route('storeMatiere')}}">
                    @csrf

                    <div class="form-group">
                        <label for="nom">Nom </label>
                        <input type="text" class="form-control" id="nom" placeholder="Entrez le nom de le complet" name="nom" required>
                        @error('nom')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>

                    <label for="niveau">Niveau</label>
                    <div class="standardSelect">

                    <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                        {{-- <option value="" label="default"></option> --}}
                        {{-- <option value= "..." >Aucun</option> --}}
                        @foreach ($niveaux as $niveau)
                            <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                        @endforeach
                    </select>
                    @error('niveau')
                      <p class="text text-danger">{{$message}}</p>
                    @enderror
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
