<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        {{-- <meta name="author" content="" />
        <meta http-equiv="Content-Security-Policy" content="frame-ancestors 'self' https://www.youtube.com;"> --}}

        <title>E-LEARNING</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="{{asset('accueil/assets/e-learning-icon-.jpg')}}" />
        <!-- Bootstrap icons-->
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" /> -->

        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{asset('accueil/css/styles.css')}}" rel="stylesheet" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

        <script src="https://kit.fontawesome.com/04fe12c13f.js" crossorigin="anonymous"></script>
    </head>
    <body>
    @include('accueil.navbar')



        <!-- Section-->
        @yield('content')

        @include('accueil.footer')

        <script src="{{asset('accueil/js/scripts.js')}}"></script>

        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>

          <!-- ================Start ionicon============ -->
      <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
      <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
      <!-- =========================End ionicon================= -->


    </body>
</html>
