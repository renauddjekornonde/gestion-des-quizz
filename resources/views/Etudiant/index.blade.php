<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/x-icon" href="{{asset('accueil/assets/e-learning-icon-.jpg')}}" />
    <link rel="stylesheet" href="{{asset('quizz/lib/lib.css')}}">
    <link rel="stylesheet" href="{{asset('quizz/style.css')}}">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> --}}

    <title>E-LEARNING</title>
</head>
<body>
    <div id="app-container">

        {{-- <button class="btn btn-danger float-right" onclick="window.location.reload();">Redémarrer</button> --}}
		<!-- Bouton de retour -->
		{{-- <button class="btn btn-secondary mr-2" onclick="window.history.back();">Retour</button> --}}


            <!-- Bouton de redémarrage -->
            <button onclick="window.location.reload();" id="restart">Ressayer</button>


        <!-- Bouton de retour -->
        <button id="retour" onclick="window.history.back();" style="border: white"><i class="fa-solid fa-arrow-left"></i></button>

        <div id="score-container">Score: 0</div>
        {{-- @foreach ($matiere as $matieres) --}}
        <h2 id="app-title">Quiz {{$matiere->contenu}}</h2>
        {{-- @endforeach --}}

        <div id="questions-container"></div>
    </div>

    <!-- Dans ma-vue.blade.php -->
    <div id="questions-data"
    data-questions="{{ json_encode($questionReponse) }}"
    data-reponses="{{ json_encode($reponse) }}"
    data-file1="{{ asset('quizz/main.js')}}"></div>



    <script src="{{asset('quizz/lib/lib.js')}}"></script>
    <script src="{{asset('quizz/main.js')}}"></script>

    <script src="https://kit.fontawesome.com/04fe12c13f.js" crossorigin="anonymous"></script>
</body>
</html>

