@extends('baseAdmin')
@section('content')
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
       <!-- Widgets  -->
       <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-1">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            {{-- <span class="currency float-left mr-1">$</span> --}}
                            <span class="count">{{$utilisateurs->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Apprenants</p>
                    </div><!-- /.card-left -->
                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-users"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-6">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count float-left">{{$matieres->count()}}</span><br>
                            {{-- <span>%</span> --}}
                        </h3>
                        <p class="text-light mt-1 m-0">Matieres</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-pen"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-3">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count">{{$questions->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Tuto</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-help1"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-flat-color-2">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                            <span class="count">{{$niveaux->count()}}</span>
                        </h3>
                        <p class="text-light mt-1 m-0">Niveaux</p>
                    </div><!-- /.card-left -->

                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-graph1"></i>
                    </div><!-- /.card-right -->

                </div>

            </div>
        </div>
        <!--/.col-->
    </div>
    <!-- /Widgets -->
    @if ($message= Session::get('success'))
    <div class="alert alert-info">
    {{$message}}
    </div>
@endif

        <div class="clearfix"></div>
        <!-- Orders -->
        <div class="orders">
            <div class="row">
                <div class="col-xl-10" id="centered">
                    <div class="card">
                        <div class="card-body" >
                            <h4 class="box-title" style="float:left;">Liste</h4>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newStudent" style="float:right;">New</button>
                        </div>
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Avatar</th>
                                            <th>Name</th>
                                            <th>Niveau</th>
                                            <th>Status</th>
                                            <th>Modifier</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    @foreach ($etudiants as $etudiant)
                                         <tr>
                                            <td class="serial">{{$etudiant->id}}</td>
                                            <td class="avatar">
                                                <div class="round-img">
                                                    <a href="#"><img class="rounded-circle" @if($etudiant->avatar=="user.png") src="{{asset('assets/images/user.png')}}" @else src="{{Storage::url($etudiant->avatar)}}" @endif alt="Avatar User"></a>
                                                </div>
                                            </td>
                                            <td>  <span class="name">{{$etudiant->name}}</span> </td>
                                            <td> <span class="product">{{$etudiant->userNiveau->nom}}</span> </td>
                                            <td>
                                                @if ($etudiant)
                                                @if ($etudiant->status==0)
                                                <form action="{{route('setStatut', $etudiant->id)}}" method="POST">
                                                    @csrf
                                                    @method('PATCH')
                                                    <button  style="border: none;  background: white;" type="submit">
                                                        <span class="badge badge-complete">Active</span></button>
                                                </form>
                                                @else
                                                <span class="badge badge-danger">Desactiver</span>
                                                @endif
                                                @endif
                                            </td>
                                            <td style="">
                                                @if ($etudiant)
                                                <h2  style="float: right;">

                                                    <a href="#modifierEtudiant{{$etudiant->id}}" style="color: green; text-decoration: none; " data-toggle="modal"><ion-icon name="pencil-outline"></ion-icon></a></h2>


                                                @endif
                                            </td>
                                             <!-- Fenêtre modale pour modifier étudiant -->
                                        <div class="modal" tabindex="-1" role="dialog" id="modifierEtudiant{{$etudiant->id}}">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Modifier</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <!-- Formulaire pour ajouter un nouvel étudiant -->
                                                        <form method="POST" action="{{route('updateEtudiant', $etudiant->id)}}" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('PATCH')

                                                            @error('niveau')
                                                            <div class="alert alert-danger text-center fw-bold" role="alert">
                                                                {{$message}}
                                                            </div>
                                                        @enderror
                                                            <div class="form-group">
                                                                <label for="nom">Nom </label>
                                                                <input type="text" class="form-control" id="nom" placeholder="Entrez le nom de le complet" name="nom" value="{{$etudiant->name}}" required>
                                                                @error('nom')
                                                                    <p class="text text-danger">{{$message}}</p>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="email">Email</label>
                                                                <input type="email" class="form-control" id="email" name="email" placeholder="Entrez adresse Email" value="{{$etudiant->email}}" required>
                                                                @error('email')
                                                                    <p class="text text-danger">{{$message}}</p>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="password">Password</label>
                                                                <input type="password" class="form-control" id="password" name="password" placeholder="Nouveau mot de passe"  required>
                                                                @error('password')
                                                                    <p class="text text-danger">{{$message}}</p>
                                                                @enderror
                                                            </div>


                                                            <label for="niveau">Niveau</label>
                                                            <div class="standardSelect">

                                                            <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                                                                @foreach ($niveaux as $niveau)
                                                                    @if ($etudiant->niveau_id== $niveau->id)
                                                                        <option value="{{ $niveau->id }}" selected>{{ $niveau->nom }}</option>
                                                                    @endif
                                                                    <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('sexe')
                                                            <p class="text text-danger">{{$message}}</p>
                                                            @enderror
                                                        </div>
                                                            <label for="sexe">Sexe</label>
                                                            <div class="standardSelect">

                                                            <select data-placeholder="Choose a sexe..." class="form-control" tabindex="1" name="sexe" id="sexe" required>
                                                                @if ($etudiant->sexe== "Homme")
                                                                <option value="Homme" selected>Homme</option>
                                                                @elseif ($etudiant->sexe== "Femme")
                                                                <option value="Femme" selected>Femme</option>
                                                             @endif
                                                                <option value="Homme">Homme</option>
                                                                <option value="Femme">Femme</option>
                                                            </select>
                                                            @error('sexe')
                                                            <p class="text text-danger">{{$message}}</p>
                                                            @enderror
                                                        </div> <br>

                                                        <label for="image">Image</label>
                                                        <div class="card">
                                                            @if ($etudiant->avatar=="user.png")
                                                            <img class="card-img-top" src="{{'storage/image/'.$etudiant->avatar}}" alt="Image Tuto" style="width:25%; display: block; margin: 0 auto;">
                                                            @else
                                                            <img class="card-img-top" src="{{Storage::url($etudiant->avatar)}}" alt="Image Tuto" style="width:30%; display: block; margin: 0 auto;">
                                                            @endif

                                                        </div>
                                                        <div class="">
                                                            <input type="file" id="image" name="image">
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                                                        </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->
                        </div>
                    </div> <!-- /.card -->
                </div>  <!-- /.col-lg-8 -->
            </div>
        </div>
        <!-- /.orders -->

    </div>
    <!-- .animated -->
</div>

<!-- Fenêtre modale pour ajouter un nouvel étudiant -->
<div class="modal" tabindex="-1" role="dialog" id="newStudent">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajouter un nouvel étudiant</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Formulaire pour ajouter un nouvel étudiant -->
                <form method="POST" action="{{route('storeEtudiant')}}">
                    @csrf
                    @error('niveau')
                    <div class="alert alert-danger text-center fw-bold" role="alert">
                        {{$message}}
                    </div>
                @enderror
                    <div class="form-group">
                        <label for="nom">Nom </label>
                        <input type="text" class="form-control" id="nom" placeholder="Entrez le nom de le complet" name="nom" required>
                        @error('nom')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Entrez adresse Email" required>
                         @error('email')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="mot de passe" required>
                         @error('password')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password confirmation">Password Confirmation</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="mot de passe de confirmation" required>
                         @error('password_confirmation')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>

                    <label for="niveau">Niveau</label>
                    <div class="standardSelect">

                    <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                        {{-- <option value="" label="default"></option> --}}
                        {{-- <option value= "..." >Aucun</option> --}}
                        @foreach ($niveaux as $niveau)
                            <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                        @endforeach
                    </select>
                    @error('sexe')
                      <p class="text text-danger">{{$message}}</p>
                    @enderror
                  </div>
                    <label for="sexe">Sexe</label>
                    <div class="standardSelect">

                    <select data-placeholder="Choose a sexe..." class="form-control" tabindex="1" name="sexe" id="sexe" required>
                        {{-- <option value="" label="default"></option> --}}
                        <option value="Homme">Homme</option>
                        <option value="Femme">Femme</option>
                    </select>
                    @error('sexe')
                      <p class="text text-danger">{{$message}}</p>
                    @enderror
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
