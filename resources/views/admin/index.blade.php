@extends('baseAdmin')
@section('content')
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                {{-- <span class="currency float-left mr-1">$</span> --}}
                                <span class="count">{{$utilisateurs->count()}}</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Apprenants</p>
                        </div><!-- /.card-left -->
                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-users"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-6">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count float-left">{{$matieres->count()}}</span><br>
                                {{-- <span>%</span> --}}
                            </h3>
                            <p class="text-light mt-1 m-0">Matieres</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-pen"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-3">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count">{{$questions->count()}}</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Tuto</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-help1"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-2">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count">{{$niveaux->count()}}</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Niveaux</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-graph1"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->
        </div>
        <!-- /Widgets -->
        @if ($message= Session::get('success'))
        <div class="alert alert-info">
        {{$message}}
        </div>
    @endif

        <div class="clearfix"></div>
        <!-- Orders -->
        <div class="orders">
            <div class="row">
                <div class="col-xl-10" id="centered">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="box-title">Student </h4>
                        </div>
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Avatar</th>
                                            <th>Name</th>
                                            <th>Niveau</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach ($etudiants as $etudiant)
                                         <tr>
                                            <td class="serial">{{$etudiant->id}}</td>
                                            <td class="avatar">
                                                <div class="round-img">
                                                    <a href="#"><img class="rounded-circle" src="{{asset('assets/images/user.png')}}" alt=""></a>
                                                </div>
                                            </td>
                                            <td>  <span class="name">{{$etudiant->name}}</span> </td>
                                            <td> <span class="product">{{$etudiant->userNiveau->nom}}</span> </td>
                                            <td>
                                                @if ($etudiant)
                                                @if ($etudiant->status==0)
                                                <form action="{{route('setStatut', $etudiant->id)}}" method="POST">
                                                    @csrf
                                                    @method('PATCH')
                                                    <button  style="border: none;" type="submit">
                                                        <span class="badge badge-complete">Active</span></button>
                                                </form>
                                                @else
                                                <span class="badge badge-danger">Desactiver</span>
                                                @endif
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->
                        </div>
                    </div> <!-- /.card -->
                </div>  <!-- /.col-lg-8 -->
            </div>
        </div>
        <!-- /.orders -->


    </div>
    <!-- .animated -->
</div>
@endsection
