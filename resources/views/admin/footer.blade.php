<footer class="site-footer fixed-bottom bg-light" >
    <div class="footer-inner bg-white">
        <div class="row">
            <div class="col-sm-6">
                ESITEC &copy; 2023
            </div>
            <div class="col-sm-6 text-right">
                Designed by <a href="https://colorlib.com">ESITEC</a>
            </div>
        </div>
    </div>
</footer>
