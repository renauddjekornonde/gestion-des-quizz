<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <br>
                    <br>
                    <br>
                    <a href="{{route('home')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="menu-title">Users</li><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Student</a>
                    <ul class="sub-menu children dropdown-menu">                   <li><i class="fa fa-plus"></i><a href="#modalEtudiant" data-toggle="modal" >Nouveau</a></li>
                        <li><i class="fa fa-id-badge"></i><a href="{{route('listeEtudiant')}}">Liste</a></li>
                    </ul>
                </li>
                {{-- <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Professeurs</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-plus"></i><a href="#modalAdmin" data-toggle="modal" >Nouveau</a></li>
                        <li><i class="fa fa-badge"></i><a href="{{route('listeProf')}}">Liste</a></li>
                    </ul>
                </li> --}}

                <li class="menu-title">Evenement</li><!-- /.menu-title -->

            <li class="">
                    <a href="{{route('matiere')}}" > <i class="menu-icon fa fa-book"></i>Matiere</a>
                </li>
                <li class="">
                    <a href="{{route('question')}}" > <i class="menu-icon fa fa-pencil-alt"></i>Questions</a>
                </li>

                {{-- <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Reponses</a>
                </li> --}}

                <li class="">
                    <a href="{{route('tuto')}}" > <i class="menu-icon fa fa-video-camera"></i>Tutos</a>
                </li>
                
                {{-- <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a></li>
                        <li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>
                        <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a></li>
                    </ul>
                </li> --}}
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>

	<!-- Fenêtre modale pour ajouter un nouvel étudiant -->
	<div class="modal" tabindex="-1" role="dialog" id="modalEtudiant">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Ajouter un nouvel étudiant</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- Formulaire pour ajouter un nouvel étudiant -->
                    <form method="POST" action="{{route('storeEtudiant')}}">
                        @csrf
                        @error('niveau')
                        <div class="alert alert-danger text-center fw-bold" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                        <div class="form-group">
                            <label for="nom">Nom </label>
                            <input type="text" class="form-control" id="nom" placeholder="Entrez le nom de le complet" name="nom" required>
                            @error('nom')
                                <p class="text text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Entrez adresse Email" required>
                             @error('email')
                                <p class="text text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="mot de passe" required>
                             @error('password')
                                <p class="text text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                        <label for="password confirmation">Password Confirmation</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="mot de passe de confirmation" required>
                         @error('password_confirmation')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                        </div>

                        <label for="niveau">Niveau</label>
                        <div class="standardSelect">

                        <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                            {{-- <option value="" label="default"></option> --}}
                            {{-- <option value= "..." >Aucun</option> --}}
                            @foreach ($niveaux as $niveau)
                                <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                            @endforeach
                        </select>
                        @error('niveau')
                          <p class="text text-danger">{{$message}}</p>
                        @enderror
                      </div>
                        <label for="sexe">Sexe</label>
                        <div class="standardSelect">

                        <select data-placeholder="Choose a sexe..." class="form-control" tabindex="1" name="sexe" id="sexe" required>
                            {{-- <option value="" label="default"></option> --}}
                            <option value="Homme">Homme</option>
                            <option value="Femme">Femme</option>
                        </select>
                        @error('sexe')
                          <p class="text text-danger">{{$message}}</p>
                        @enderror
                      </div>
                      <div class="modal-footer">
					    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					    <button type="submit" class="btn btn-primary">Enregistrer</button>
				      </div>
                    </form>
				</div>

			</div>
		</div>
	</div>

	<!-- Fenêtre modale pour ajouter un nouveau professeur-->
	<div class="modal" tabindex="-1" role="dialog" id="modalAdmin">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Ajouter un nouveau Professeur</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- Formulaire pour ajouter un nouvel étudiant -->
                    <form method="POST" action="{{route('storeProfesseur')}}">
                        @csrf
                        @error('niveau')
                        <div class="alert alert-danger text-center fw-bold" role="alert">
                            {{$message}}
                        </div>
                    @enderror
                        <div class="form-group">
                            <label for="nom">Nom </label>
                            <input type="text" class="form-control" id="nom" placeholder="Entrez le nom de le complet" name="nom" required>
                            @error('nom')
                                <p class="text text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Entrez adresse Email" required>
                             @error('email')
                                <p class="text text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="mot de passe" required>
                             @error('password')
                                <p class="text text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                        <label for="password confirmation">Password Confirmation</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="mot de passe de confirmation" required>
                         @error('password_confirmation')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>

                        <label for="niveau">Niveau</label>
                        <div class="standardSelect">

                        <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                            {{-- <option value="" label="default"></option> --}}
                            {{-- <option value= "..." >Aucun</option> --}}
                            @foreach ($niveaux as $niveau)
                                <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                            @endforeach
                        </select>
                        @error('niveau')
                          <p class="text text-danger">{{$message}}</p>
                        @enderror
                      </div>
                        <label for="sexe">Sexe</label>
                        <div class="standardSelect">

                        <select data-placeholder="Choose a sexe..." class="form-control" tabindex="1" name="sexe" id="sexe" required>
                            {{-- <option value="" label="default"></option> --}}
                            <option value="Homme">Homme</option>
                            <option value="Femme">Femme</option>
                        </select>
                        @error('sexe')
                          <p class="text text-danger">{{$message}}</p>
                        @enderror
                      </div>
                      <div class="modal-footer">
					    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					    <button type="submit" class="btn btn-primary">Enregistrer</button>
				     </div>
                    </form>
				</div>

			</div>
		</div>
	</div>

