@extends('baseAdmin')
@section('content')
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="currency float-left mr-1">$</span>
                                <span class="count">23569</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Revenue</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-cart"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-6">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count float-left">85</span>
                                <span>%</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Dummy text here</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <div id="flotBar1" class="flotBar1"></div>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-3">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count">6569</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Total clients</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-users"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-2">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count">1490</span>
                            </h3>
                            <p class="text-light mt-1 m-0">New users</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <div id="flotLine1" class="flotLine1"></div>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->
        </div>
        <!-- /Widgets -->

        <div class="clearfix"></div>
        <!-- Orders -->
        <div class="orders">
            <div class="row">
                <div class="col-xl-10" id="centered">
                    <div class="card">
                        <div class="card-body" >
                            <h4 class="box-title" style="float:left;">Orders</h4>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addProf" style="float:right;">New</button>
                        </div>
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Avatar</th>
                                            <th>Name</th>
                                            <th>Niveau</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($professeurs as $professeur)
                                        <tr>
                                           <td class="serial">{{$professeur->id}}</td>
                                           <td class="avatar">
                                               <div class="round-img">
                                                   <a href="#"><img class="rounded-circle" src="{{asset('assets/images/user.png')}}" alt=""></a>
                                               </div>
                                           </td>
                                           <td>  <span class="name">{{$professeur->name}}</span> </td>
                                           <td> <span class="product">{{$professeur->niveau}}</span> </td>
                                           <td>
                                               <span class="badge badge-complete">Active</span>
                                           </td>
                                       </tr>
                                   @endforeach
                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->
                        </div>
                    </div> <!-- /.card -->
                </div>  <!-- /.col-lg-8 -->
            </div>
        </div>
        <!-- /.orders -->

    </div>
    <!-- .animated -->
</div>

<!-- Fenêtre modale pour ajouter un nouvel professeur -->
<div class="modal" tabindex="-1" role="dialog" id="addProf">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajouter un Professeur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Formulaire pour ajouter un nouvel étudiant -->
                <form method="POST" action="{{route('storeProfesseur')}}">
                    @csrf
                    <div class="form-group">
                        <label for="nom">Nom </label>
                        <input type="text" class="form-control" id="nom" placeholder="Entrez le nom de le complet" name="nom" required>
                        @error('nom')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Entrez adresse Email" required>
                         @error('email')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="mot de passe" required>
                         @error('password')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                       <div class="form-group">
                        <label for="password confirmation">Password Confirmation</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="mot de passe de confirmation" required>
                         @error('password_confirmation')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>

                    <label for="niveau">Niveau</label>
                    <div class="standardSelect">

                    <select data-placeholder="Choose a niveau..." class="form-control" tabindex="1" name="niveau" id="niveau" required>
                        {{-- <option value="" label="default"></option> --}}
                        <option value= "..." >Aucun</option>
                        @foreach ($niveaux as $niveau)
                            <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                        @endforeach
                    </select>
                    @error('niveau')
                      <p class="text text-danger">{{$message}}</p>
                    @enderror
                  </div>
                    <label for="sexe">Sexe</label>
                    <div class="standardSelect">

                    <select data-placeholder="Choose a sexe..." class="form-control" tabindex="1" name="sexe" id="sexe" required>
                        {{-- <option value="" label="default"></option> --}}
                        <option value="Homme">Homme</option>
                        <option value="Femme">Femme</option>
                    </select>
                    @error('sexe')
                      <p class="text text-danger">{{$message}}</p>
                    @enderror
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
