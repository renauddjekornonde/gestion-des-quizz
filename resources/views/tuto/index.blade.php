@extends('baseAdmin')
@section('content')
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                {{-- <span class="currency float-left mr-1">$</span> --}}
                                <span class="count">{{$etudiants->count()}}</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Apprenants</p>
                        </div><!-- /.card-left -->
                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-users"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-6">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count float-left">{{$matieres->count()}}</span><br>
                                {{-- <span>%</span> --}}
                            </h3>
                            <p class="text-light mt-1 m-0">Matieres</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-pen"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-3">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count">{{$questions->count()}}</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Tuto</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-help1"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-2">
                    <div class="card-body">
                        <div class="card-left pt-1 float-left">
                            <h3 class="mb-0 fw-r">
                                <span class="count">{{$niveaux->count()}}</span>
                            </h3>
                            <p class="text-light mt-1 m-0">Niveaux</p>
                        </div><!-- /.card-left -->

                        <div class="card-right float-right text-right">
                            <i class="icon fade-5 icon-lg pe-7s-graph1"></i>
                        </div><!-- /.card-right -->

                    </div>

                </div>
            </div>
            <!--/.col-->
        </div>
        <!-- /Widgets -->
        @if ($message= Session::get('success'))
            <div class="alert alert-info">
            {{$message}}
            </div>
        @endif

        {{-- Content --}}
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Tutos</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    {{-- <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">UI Elements</a></li>
                                    <li class="active">Cards</li> --}}
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addTuto" >New</button>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                   @foreach ($tutos as $tuto)
                    <div class="col-md-3">
                        <div class="card">
                            <iframe width="100%" height="215" src="{{$tuto->contenu}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            <div class="card-body">
                                <h3 class="card-title mb-3"><a  data-toggle="modal" href="#show{{$tuto->id}}">{{$tuto->titre}}</a></h3>

                                <p class="card-text clipped-text" title="{{$tuto->description}}">{{$tuto->description}}</p>

                                <!-- Fenêtre modale pour SHOW un tuto -->
                                <div class="modal" tabindex="-1" role="dialog" id="show{{$tuto->id}}">
                                    <div class="modal-dialog" role="document" style=" width: auto; margin-top:10%">
                                        <!--  -->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">{{$tuto->titre}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body" >
                                                    <div class="card">
                                                        <img class="card-img-top" src="{{Storage::url($tuto->image)}}" alt="Image Tuto" style="width:30%; display: block; margin: 0 auto;">
                                                        <div class="card-body">

                                                            <h4 class="card-title mb-3 few-bold">Matiere :{{"   ".$tuto->tutoMatiere->contenu}}</h4>


                                                            <h4 class="card-title mb-3 few-bold">Lien : <a href="{{$tuto->contenu}}" style="color:rgba(98, 219, 209, 0.979);">Cliquez Ici</a></h4>

                                                            <h4 class="card-title mb-2">Description :</h4>
                                                            <p class="card-text">{{$tuto->description}}.</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href="#tuto{{$tuto->id}}" data-toggle="modal"  data-dismiss="modal" class="btn btn-success">Modifier</a>

                                                            <form action="{{route('destroyTuto', $tuto->id)}}" method="POST">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" class="btn btn-danger">Supprimer</button>
                                                            </form>
                                                         </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Fenêtre modale pour Modifier tuto -->
                        <div class="modal" tabindex="-1" role="dialog" id="tuto{{$tuto->id}}">
                            <div class="modal-dialog" role="document"  style=" width: auto; margin-top:0%">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Modifer Tuto</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST" action="{{route('updateTuto', $tuto->id)}}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PATCH')
                                            <div class="form-group">
                                                <label for="titre">Titre </label>
                                                <input type="text" class="form-control" id="titre" placeholder="Titre de la video" name="titre" value="{{$tuto->titre}}" required>
                                                @error('titre')
                                                    <p class="text text-danger">{{$message}}</p>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <textarea class="form-control" id="description" rows="4" placeholder="Entrez le contenu de l'actualité" name="description" required>{{$tuto->description}}</textarea>
                                                @error('description')
                                                    <p class="text text-danger">{{$message}}</p>
                                                @enderror
                                            </div>

                                            <label for="niveau">Matiere</label>
                                            <div class="standardSelect">

                                            <select data-placeholder="Choose a niveau..." class="form-control @error('matiere') is-invalid @enderror" tabindex="1" name="matiere" id="matiere" required>
                                                @foreach ($matieres as $matiere)
                                                    @if ($tuto->matiere_id== $matiere->id)
                                                        <option value="{{ $matiere->id }}" selected>{{ $matiere->contenu }}</option>
                                                    @endif
                                                    <option value="{{ $matiere->id }}">{{ $matiere->contenu }}</option>
                                                @endforeach
                                            </select>
                                            @error('matiere')
                                            <p class="text text-danger">{{$message}}</p>
                                            @enderror
                                        </div>

                                        <label for="niveau">Niveau</label>
                                        <div class="standardSelect">

                                        <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                                            @foreach ($niveaux as $niveau)
                                                @if ($tuto->niveau_id== $niveau->id)
                                                <option value="{{ $niveau->id }}" selected>{{ $niveau->nom }}</option>
                                                @endif
                                                <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                                            @endforeach
                                        </select>
                                        @error('niveau')
                                            <p class="text text-danger">{{$message}}</p>
                                        @enderror
                                        </div><br>
                                            <div class="form-group">
                                                <label for="thumbnail">Thumbnail</label>
                                                <div class="card">
                                                    <img class="card-img-top" src="{{Storage::url($tuto->image)}}" alt="Image Tuto" style="width:30%; display: block; margin: 0 auto;">
                                                </div>
                                                <input type="file" class="form-control" id="image" name="image" value="{{$tuto->image}}">
                                                @error('image')
                                                    <p class="text text-danger">{{$message}}</p>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="lien video">Lien Video </label>
                                                <input type="text" class="form-control" id="contenu" placeholder="Inserer le lien youtube" name="contenu" value="{{$tuto->contenu}}" required>
                                                @error('contenu')
                                                    <p class="text text-danger">{{$message}}</p>
                                                @enderror
                                            </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                                        </div>
                                        </form>
                                    </div>

                                </div>
                            </div>

                        </div>
                   @endforeach
                </div>
            </div>
        </div>
        {{-- End content --}}

    </div>
</div>
<!-- Fenêtre modale pour ajouter un nouveau tuto -->
<div class="modal" tabindex="-1" role="dialog" id="addTuto">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nouveau Tuto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Formulaire pour ajouter un nouvel étudiant -->
                <form method="POST" action="{{route('storeTuto')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="titre">Titre </label>
                        <input type="text" class="form-control" id="titre" placeholder="Titre de la video" name="titre" required>
                        @error('titre')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                         <textarea class="form-control" id="description" rows="4" placeholder="Entrez le contenu de l'actualité" name="description" required></textarea>
                         @error('description')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>

                    <label for="niveau">Matiere</label>
                    <div class="standardSelect">

                    <select data-placeholder="Choose a niveau..." class="form-control @error('matiere') is-invalid @enderror" tabindex="1" name="matiere" id="matiere" required>
                        @foreach ($matieres as $matiere)
                            <option value="{{ $matiere->id }}">{{ $matiere->contenu }}</option>
                        @endforeach
                    </select>
                    @error('matiere')
                      <p class="text text-danger">{{$message}}</p>
                    @enderror
                  </div>

                  <label for="niveau">Niveau</label>
                  <div class="standardSelect">

                  <select data-placeholder="Choose a niveau..." class="form-control @error('niveau') is-invalid @enderror" tabindex="1" name="niveau" id="niveau" required>
                      @foreach ($niveaux as $niveau)
                          <option value="{{ $niveau->id }}">{{ $niveau->nom }}</option>
                      @endforeach
                  </select>
                  @error('niveau')
                    <p class="text text-danger">{{$message}}</p>
                  @enderror
                </div><br>
                    <div class="form-group">
                        <label for="thumbnail">Thumbnail</label>
                        <input type="file" class="form-control" id="image" name="image" required>
                         @error('image')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="lien video">Lien Video </label>
                        <input type="text" class="form-control" id="contenu" placeholder="Inserer le lien youtube" name="contenu" required>
                        @error('contenu')
                            <p class="text text-danger">{{$message}}</p>
                        @enderror
                    </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
                </form>
            </div>

        </div>
    </div>

</div>

@endsection
