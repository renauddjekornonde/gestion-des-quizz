/*!
* Start Bootstrap - Shop Homepage v5.0.5 (https://startbootstrap.com/template/shop-homepage)
* Copyright 2013-2022 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-shop-homepage/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project


// Récupération des boutons
var btn1 = document.getElementById("btn1");
var btn2 = document.getElementById("btn2");
var btn3 = document.getElementById("btn3");

// Récupération des sections
var section1 = document.getElementById("section1");
var section2 = document.getElementById("section2");
var section3 = document.getElementById("section3");

// Fonction pour afficher la section 1
function showSection1() {
    // Masquer toutes les sections
    section1.classList.add("py-5");
    section2.classList.remove("py-5");
    section3.classList.remove("py-5");
}

// Fonction pour afficher la section 2
function showSection2() {
    // Masquer toutes les sections
    section1.classList.remove("py-5");
    section2.classList.add("py-5");
    section3.classList.remove("py-5");
}

// Fonction pour afficher la section 3
function showSection3() {
    // Masquer toutes les sections
    section1.classList.remove("py-5");
    section2.classList.remove("py-5");
    section3.classList.add("py-5");
}

// Ajout des écouteurs d'événements sur les boutons
btn1.addEventListener("click", showSection1);
btn2.addEventListener("click", showSection2);
btn3.addEventListener("click", showSection3);



  let modalId = "";

  var coursClicked = false;




  document.getElementById("btnCours").addEventListener("click", function() {
  modalId = this.getAttribute("href"); // récupère la valeur de l'attribut href qui contient l'ID du modal
  });

  document.getElementById("btnCours").addEventListener("click", function() {
  coursClicked = true;
  });

  document.getElementById("btnQuizz").addEventListener("click", function(event) {
  if (!coursClicked) {
      event.preventDefault(); // empêche l'action par défaut du lien
      alert("Veuillez suivre le cours avant de passer au quizz."); // affiche un message d'erreur
      modalId.modal('show');
  }
  });

