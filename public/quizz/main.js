//
// main.js
//



const questionsDataElement = document.getElementById('questions-data');

const questionData = JSON.parse(questionsDataElement.getAttribute('data-questions'));

const reponsesData = JSON.parse(questionsDataElement.getAttribute('data-reponses'));

if(Array.isArray(questionData)) {
  questionData.forEach(function(question) {
      console.log(question);
  });
} else {
  console.error('utilisateursData n\'est pas un tableau');
}

if(Array.isArray(reponsesData)) {
  reponsesData.forEach(function(reponse) {
      console.log(reponse['id']);
  });
} else {
  console.error('utilisateursData n\'est pas un tableau');
}

let questionsData = [];

questionData.forEach(question=>{
    const questionObj= {
        text: question.contenu,
        answers: []
    };

    reponsesData.forEach(reponse =>{
        if(question['id']==reponse['question_id']){
            questionObj.answers.push({
                text:reponse.contenu,
                isCorrect:reponse.status
            });
        }
    });
    questionsData.push(questionObj);
});



// variables initialization
let questions = [];
let score = 0, answeredQuestions = 0;
let appContainer = document.getElementById("questions-container");
let scoreContainer = document.getElementById("score-container");
scoreContainer.innerHTML = `Score: ${score}/${questionsData.length}`;

/**
 * Shuffles array in place. ES6 version
 * @param {Array} arr items An array containing the items.
 */
function shuffle(arr) {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
}

shuffle(questionsData);

// creating questions
for (var i = 0; i < questionsData.length; i++) {
  let question = new Question({
    text: questionsData[i].text,
    answers: questionsData[i].answers
  });

  appContainer.appendChild(question.create());
  questions.push(question);
}

document.addEventListener("question-answered", ({ detail }) => {
  if (detail.answer.isCorrect=="vraie") {
    score++;
  }

  answeredQuestions++;
  scoreContainer.innerHTML = `Score: ${score}/${questions.length}`;
  detail.question.disable();

  if (answeredQuestions == questions.length) {
    setTimeout(function () {
      alert(`Quiz completed! \nFinal score: ${score}/${questions.length}`);
    }, 100);
  }
});

// const restartBtn= document.querySelector('#restart');
// restartBtn.addEventListener('click', ()=>{
//     shuffle(questionsData);
//     console.log(questions, questionsData);
// });

console.log(questions, questionsData);

