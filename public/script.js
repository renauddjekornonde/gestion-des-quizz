//function myfunc(){
//    alert(document.getElementById("language").value);
//}

var countQues=0;
var lang;
var score=0;
var correct=0;
var incorrect=0;
var resultats=[];
const questionsDataElement = document.getElementById('questions-data');

const questionData = JSON.parse(questionsDataElement.getAttribute('data-questions'));

const reponsesData = JSON.parse(questionsDataElement.getAttribute('data-reponses'));

if(Array.isArray(questionData)) {
  questionData.forEach(function(question) {
    //   console.log(question);
  });
} else {
  console.error('utilisateursData n\'est pas un tableau');
}

if(Array.isArray(reponsesData)) {
  reponsesData.forEach(function(reponse) {
    //   console.log(reponse['id']);
  });
} else {
  console.error('utilisateursData n\'est pas un tableau');
}

// let questionsData = [];

// questionData.forEach(question=>{
//     const questionObj= {
//         text: question.contenu,
//         answers: []
//     };

//     reponsesData.forEach(reponse =>{
//         if(question['id']==reponse['question_id']){
//             questionObj.answers.push({
//                 text:reponse.contenu,
//                 isCorrect:reponse.status
//             });
//         }
//     });
//     questionsData.push(questionObj);
// });

let mesQuestions= [];
questionData.forEach(questions=>{
    const questionObj={
        question: questions.contenu,
        choices:[],
    };
    reponsesData.forEach(reponse =>{
        if(questions['id']==reponse['question_id']){
            questionObj.choices.push({
                text:reponse.contenu,
                answer:reponse.status
            });
            // questionObj.answer.push({
            //     text:reponse.contenu,
            //     answer:reponse.status
            // });
        }
    });
    mesQuestions.push(questionObj);

});

// console.log(mesQuestions[countQues].answer[1])
// var mesQuestion=[

//     {
//         question: "C Language was developed in the year ____",
//         choices: ["1970","1975","1980","1985"],
//         answer: 1

//     },
//     {
//         question: "Which one is not a reserve keyword in C Language?",
//         choices: ["auto","main","case","register"],
//         answer: 2

//     },
//     {
//         question: "A C variable name can start with a ____",
//         choices: ["Number","Plus Sign","Underscore","Asterisk"],
//         answer: 3

//     },
//     {
//         question: "Prototype of a function means _____",
//         choices: ["Name of Function","Output of Function","Declaration of Function","Input of a Function"],
//         answer: 3

//     },
//     {
//         question: "Name the loop that executes at least once.",
//         choices: ["for","If","do-while","while"],
//         answer: 3

//     },
//     {
//         question: "Far pointer can access _____",
//         choices: ["Single memory location","No memory location","All memory location","First and Last Memory Address"],
//         answer: 3

//     },
//     {
//         question: "A pointer pointing to a memory location of the variable even after deletion of the variavle is known as _____",
//         choices: ["far pointer","dangling pointer","null pointer","void pointer"],
//         answer: 2

//     },
//     {
//         question: "An uninitialized pointer in C is called ___",
//         choices: ["Constructor","dangling pointer","Wild Pointer","Destructor"],
//         answer: 3

//     },
//     {
//         question: "A pointer that is pointing to NOTHING is called ____",
//         choices: ["VOID Pointer","DANGLING Pointer","NULL Pointer","WILD Pointer"],
//         answer: 3

//     },
//     {
//         question: "Who is known as the father of C Language ?",
//         choices: ["Digvijay","James A. Sosling","Dr. E. F. Codd","Dennis Ritchie"],
//         answer: 4

//     }

// ];





//alert(questions.length);
document.getElementById("score").textContent="Score : "+0;
document.querySelector(".view-results").style.display="none";
document.querySelector(".quiz").style.display="none";
document.querySelector(".final-result").style.display="none";








// document.querySelector(".choose-lang").addEventListener("click",function(){

//     lang=document.getElementById("language").value+"questions";
//     document.getElementById("ques-left").textContent="Question : "+(countQues+1)+"/"+window[lang].length;

// //    alert(window[lang].length);
//     //window["anything"] will convert "anything" string to object because window is also an object
//     document.querySelector(".quiz").style.display="block";

//     document.querySelector(".question").innerHTML="<h1>"+window[lang][countQues].question+"</h1>";
//      for (i=0;i<=3;i++){
//         document.getElementById("opt"+i).value=window[lang][countQues].choices[i];
//         document.getElementById("lb"+i).innerHTML=window[lang][countQues].choices[i];

//     };/*For loop Closed*/

// //    window.location.href="#score";

// });


// Utilisez directement l'objet "questions" pour afficher les questions
document.getElementById("ques-left").textContent="Question : "+(countQues+1)+"/"+mesQuestions.length;

// Utilisez directement l'objet "questions" pour afficher la question
document.querySelector(".quiz").style.display="block";
document.querySelector(".question").innerHTML="<h1>"+mesQuestions[countQues].question+"</h1>";
let j=0;
for (i=0; i < mesQuestions[countQues].choices.length; i++){
    document.getElementById("opt" + i).value = mesQuestions[countQues].choices[i].text;
    document.getElementById("lb" + i).innerHTML = mesQuestions[countQues].choices[i].text;
    j=j+1;
};
for(j; j<3; j++){
    document.getElementById("opt" + j).style.display="none";
    document.getElementById("lb" + j).style.display="none";
}








document.querySelector(".submit-answer").addEventListener("click",function(){
//     alert(window[lang][countQues].choices[window[lang][countQues].answer-1]);
//     alert(document.querySelector('input[name="options"]:checked').value);

let quest= document.querySelector('input[name="options"]:checked').value;
// console.log(quest)
let quest_answer= "";

reponsesData.forEach(reponse =>{
    if(quest==reponse['contenu']){
       quest_answer= reponse['status'];
    }
});
    if(quest_answer=="vraie"){

        score+=10;
        document.getElementById("score").textContent="Score : "+score;
        // document.getElementById("ques-view").innerHTML+="<div class='ques-circle correct'>"+(countQues+1)+"</div>";
        correct= correct+1;
        resultats.push("correct");

    }else{

        score-=5;
        document.getElementById("score").textContent="Score : "+score;
        // document.getElementById("ques-view").innerHTML+="<div class='ques-circle incorrect'>"+(countQues+1)+"</div>";
        incorrect= incorrect+1
        resultats.push("incorrect");
    };

    if (countQues<mesQuestions.length-1){
        countQues++;
    }else{
        document.querySelector(".submit-answer").style.display="none";
        document.querySelector(".view-results").style.display="unset";

    }

    document.getElementById("ques-left").textContent="Question : "+(countQues+1)+"/"+mesQuestions.length;
    document.querySelector(".question").innerHTML="<h1>"+mesQuestions[countQues].question+"</h1>";
    for (i=0; i < mesQuestions[countQues].choices.length; i++){
        document.getElementById("opt" + i).value = mesQuestions[countQues].choices[i].text;
        document.getElementById("lb" + i).innerHTML = mesQuestions[countQues].choices[i].text;
        j=j+1;
    };/*For loop Closed*/

});

document.querySelector(".view-results").addEventListener("click",function(){

    document.querySelector(".final-result").style.display="block";

    document.querySelector(".solved-ques-no").innerHTML="Vous avez résolu "+(countQues+1)+" questions";

    // var correct= document.getElementById("ques-view").getElementsByClassName("correct").length;

    // document.querySelector(".right-wrong").innerHTML=correct+" correct and "+((countQues+1)-correct)+" incorrect";
    document.querySelector(".right-wrong").innerHTML=correct+" réponses juste et "+incorrect+" réponse incorrect ";

    document.getElementById("display-final-score").innerHTML="Votre score final est: "+score;

    if (correct/(countQues+1)>0.8){
        document.querySelector(".remark").innerHTML="Remarque : Exceptionnel ! :)";
    }else if(correct/(countQues+1)>0.6){
        document.querySelector(".remark").innerHTML="Remarque : Bien, continuez à vous améliorer.";

    }else if(correct/(countQues+1)){
        document.querySelector(".remark").innerHTML="Remarque : Satisfaisant, En savoir plus.";
    }else{
        document.querySelector(".remark").innerHTML="Remarque : insatisfaisant, réessayez.";
    }

//    window.location.href="#display-final-score";
for (var i = 0; i < resultats.length; i++) {
    if (resultats[i] === "correct") {
        document.getElementById("result-view").innerHTML += "<div class='ques-circle correct'>"+(i+1)+"</div>";
    } else {
        document.getElementById("result-view").innerHTML += "<div class='ques-circle incorrect'>"+(i+1)+"</div>";
    }
}


});

document.getElementById("restart").addEventListener("click",function(){
    location.reload();

});

document.getElementById("about").addEventListener("click",function(){
    alert("Quiz Website Project Created By Renaud and Abdou");

});


/*Smooth Scroll*/


$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
});



/*Smooth Scroll End*/
