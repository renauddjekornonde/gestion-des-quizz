<?php

namespace App\Http\Controllers;

use App\Models\Etudiant;
use App\Models\Matiere;
use App\Models\Niveau;
use App\Models\Question;
use App\Models\Reponse;
use App\Models\User;
use App\Models\Tuto;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $etudiants= User::where([['email','!=',"admin@supdeco.edu.sn"]])->get();
        $utilisateurs= User::where('status', 0)->get();
        $niveaux= Niveau::all();
        $matieres= Matiere::all();
        $questions= Tuto::all();

        return view('admin.index', compact('etudiants', 'niveaux','utilisateurs','matieres', 'questions'));
    }

    public function indexquizz($id){

        $matiere= Matiere::findOrFail($id);

        $questionReponse= Question::where('matiere_id', $matiere->id)->get();

        $reponse= Reponse::get();


        // dump($reponse);

        return view('testequizz.index', ['questionReponse'=>$questionReponse, 'reponse'=>$reponse], compact('matiere'));
        // return view('Etudiant.index', ['questionReponse'=>$questionReponse, 'reponse'=>$reponse], compact('matiere'));
    }

    public function accueil(){
        $tutos= Tuto::get();
        return view('accueil.index', compact('tutos'));
    }
    public function service(){
        $tutos= Tuto::get();
        return view('accueil.service', compact('tutos'));
    }

}
