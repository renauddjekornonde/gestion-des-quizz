<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Niveau;
use App\Models\User;
use App\Models\Matiere;
use App\Models\Question;
use App\Models\Tuto;

class MatiereController extends Controller
{
    //
    public function index(){
        $etudiants= User::where('status', 0)->get();
        $niveaux= Niveau::all();
        $matieres= Matiere::with('matiereNiveau')->get();
        $niveaux= Niveau::all();
        // $matiere= Matiere::all();
        $questions= Tuto::all();
        return view('matiere.index', compact('etudiants', 'niveaux','etudiants','matieres','questions'));
    }

    public function store(Request $request){
        $matiere= new Matiere();
        $matiere->contenu= $request['nom'];
        $matiere->niveau_id= $request['niveau'];
        $matiere->save();

        return redirect()->back()->with('success','Matiere ajouter avec succes');
    }

     public function destroy($id)
    {
        Matiere::find($id)->delete();

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {

        $matieres= Matiere::find($id);
        $matieres->contenu= $request->nom;
        $matieres->niveau_id= $request->niveau;

        $matieres->save();


        return redirect()->back()->with('success', 'Modification Effectuer Avec Succes');
    }
}
