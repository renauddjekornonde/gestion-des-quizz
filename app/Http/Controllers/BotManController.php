<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use BotMan\BotMan\Messages\Incoming\Answer;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->hears('{message}', function ($botman, $message) {

            if ($message == 'Salut') {
                $this->askName($botman);
            } else {
                $botman->reply("En tant que Assistante je ne suis pas en mesure de comprendre ce que vous dîtes.");
            }
        });

        $botman->listen();
    }

    /**
     * Place your BotMan logic here.
     */
    public function askName($botman)
    {
        $botman->ask('Bonjour! Comment vous vous appelez?', function (Answer $answer) {

            $name = $answer->getText();

            $this->say('Enchantée ' . $name. ' que puis-je faire pour vous?');
        });
    }
}
