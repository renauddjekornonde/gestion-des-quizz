<?php

namespace App\Http\Controllers;

use App\Http\Requests\EtudiantStore;
use App\Models\Niveau;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfesseurController extends Controller
{
    //
    public function store(EtudiantStore $request){

        $input = $request->validated();


        $etudiants= new User();
            $etudiants->name=$input['nom'];
            $etudiants->email=$input['email'];
            $etudiants->niveau_id=$input['niveau'];
            $etudiants->sexe=$input['sexe'];
            $etudiants->status=1;
            $etudiants->password= Hash::make($input['password']);

            $etudiants->save();

        return redirect()->back();
    }


    public function index(){
        $professeurs= User::where('status', 1)->get();
        $niveaux= Niveau::all();

        return view('professeur.liste', compact('professeurs', 'niveaux'));
    }

    //fonction permettant de recupere la liste des etudiant selon le niveau de chaque professeur
    public function indexProf(){
        $niveau= Auth::user()->niveau_id;
        $etudiants= User::where([['status','=', 0],['niveau_id','=',$niveau]])->get();
        $niveaux= Niveau::all();

        return view('professeur.index', compact('etudiants', 'niveaux'));
    }


   
}
