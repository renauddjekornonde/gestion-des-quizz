<?php

namespace App\Http\Controllers;

use App\Http\Requests\EtudiantStore;
use App\Models\Etudiant;
use App\Models\Matiere;
use App\Models\Niveau;
use App\Models\Question;
use App\Models\Tuto;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class EtudiantController extends Controller
{
    //
    public function store(EtudiantStore $request){

        $input = $request->validated();
        $niveau= Niveau::all();


        $etudiants= new User();
            $etudiants->name=$input['nom'];
            $etudiants->email=$input['email'];

            if($niveau->count()==0){
                $niveau1= new Niveau();
                $niveau1->nom= "Debutant";
                $niveau1->save();

                $niveau2= new Niveau();
                $niveau2->nom= "Elementaire";
                $niveau2->save();

                $niveau3= new Niveau();
                $niveau3->nom= "Intermediaire";
                $niveau3->save();
            }

            $etudiants->niveau_id=$input['niveau'];
            $etudiants->sexe=$input['sexe'];
            $etudiants->status=0;
            $etudiants->password= Hash::make($input['password']);

            $etudiants->save();

        return redirect()->back();
    }



    public function index(){
        $etudiants= User::where([['email','!=',"admin@supdeco.edu.sn"]])->get();
        $utilisateurs= User::where('status', 0)->get();
        $niveaux= Niveau::all();
        $matieres= Matiere::all();
        $questions= Tuto::all();

        return view('Etudiant.liste', compact('etudiants', 'niveaux','matieres','questions','utilisateurs'));
    }

    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $etudiants= User::find($id);
        $etudiants->name= $request->nom;
        $etudiants->email= $request->email;
        $etudiants->niveau_id= $request->niveau;
        $etudiants->sexe= $request->sexe;
        $etudiants->password= Hash::make($request->password);
        $etudiants->status= 0;

        $images= $request->hasFile('image');

        if($images){
            $newFileImage= $request->file('image');
            $file_path_image= $newFileImage->store('public/images/');

            $etudiants->avatar= $file_path_image;

        }
        $etudiants->save();


        return redirect()->back()->with('success', 'Modification Effectuer Avec Succes');
    }

    public function setStatut($id){
        $etudiants= User::find($id);
        $etudiants->status= 2;
        $etudiants->save();
        return redirect()->back();
    }
    public function edit_profil(){
        return view('accueil.edit_profil');
    }
    public function update_edit_profil(Request $request, $id){
        $user = User::find($id);
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();
        return redirect()->back()->with('success', 'Profil mis à jour avec succès.');
    }

}
