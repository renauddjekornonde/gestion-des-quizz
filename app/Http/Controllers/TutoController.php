<?php

namespace App\Http\Controllers;

use App\Models\Matiere;
use App\Models\Niveau;
use App\Models\Question;
use App\Models\Tuto;
use App\Models\User;
use Illuminate\Http\Request;

class TutoController extends Controller
{
    //
    public function index(){
        $etudiants= User::where('status', 0)->get();
        $professeurs= User::where('status', 1)->get();
        $niveaux= Niveau::all();
        $matieres= Matiere::all();
        $tutos= Tuto::all();
        $questions= Tuto::all();
        return view('tuto.index', compact('etudiants', 'niveaux','professeurs','matieres','tutos','questions'));
    }

    public function store(Request $request){

            $photos= new Tuto();
            $images= $request->hasFile('image');

            if($images){
                $newFileImage= $request->file('image');
                $file_path_image= $newFileImage->store('public/images/');

                  $photos->contenu= $request['contenu'];
                  $photos->image= $file_path_image;
                  $photos->matiere_id= $request['matiere'];
                  $photos->niveau_id= $request['niveau'];
                  $photos->titre= $request['titre'];
                  $photos->description= $request['description'];
                  $photos->save();
            }


        return redirect()->back()->with('success','Video enregistrer avec succes');

    }

    public function destroy($id)
    {
        Tuto::find($id)->delete();

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {

        $tutos= Tuto::find($id);
        $tutos->contenu= $request->contenu;
        $tutos->matiere_id= $request->matiere;
        $tutos->niveau_id= $request->niveau;
        $tutos->titre= $request->titre;
        $tutos->description= $request->description;
        $images= $request->hasFile('image');

        if($images){
            $newFileImage= $request->file('image');
            $file_path_image= $newFileImage->store('public/images/');
            $tutos->image= $file_path_image;
        }

        $tutos->save();


        return redirect()->back()->with('success', 'Modification Effectuer Avec Succes');
    }
}
