<?php

namespace App\Http\Controllers;

use App\Models\Reponse;
use Illuminate\Http\Request;

class ReponseController extends Controller
{
    //

    public function update(Request $request, $id)
    {

        $reponses= Reponse::find($id);
        $reponses->contenu= $request->contenu;
        $reponses->status= $request->status;

        $reponses->save();


        return redirect()->back()->with('success', 'Modification Effectuer Avec Succes');
    }

    public function destroy($id)
    {
        Reponse::find($id)->delete();

        return redirect()->back();
    }
    public function store(Request $request)
    {
        $data=$request->all();
        for($i=0; $i< count($data['contenuReponse']); $i++){

            // echo $i;
            $reponse = new Reponse();

          $reponse->question_id = $request['id_question'];
          $reponse->status= $data['status'][$i];
          $reponse->contenu = $data['contenuReponse'][$i];
    //   dd($operation);
          $reponse->save();
        }

        return redirect()->back()->with('success','Reponse ajouter avec succès');
    }
}
