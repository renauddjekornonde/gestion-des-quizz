<?php

namespace App\Http\Controllers;

use App\Models\MescOurs;
use App\Models\Tuto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MescOursController extends Controller
{
    public function storeMesCours(Request $request){
       
        $userId = Auth::user()->id;

        $mesCours = new MescOurs();
        $mesCours->user_id = $userId;
        $mesCours->tuto_id = $request['id'];
        $mesCours->save();

        return redirect()->back()->with('success','Favorie ajouter avec succès');
    
  }

  public function index(){
    $id_user= Auth::user()->id;
    $mescours= MescOurs::where('user_id', $id_user)->get();
    $tutos= Tuto::all();
    return view('Etudiant.mescours',compact('mescours','tutos'));
  }

}
