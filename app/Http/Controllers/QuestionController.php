<?php

namespace App\Http\Controllers;

use App\Models\Matiere;
use App\Models\Niveau;
use App\Models\Question;
use App\Models\Reponse;
use App\Models\Tuto;
use App\Models\User;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    //
    public function index(){
        $etudiants= User::where('status', 0)->get();

        $niveaux= Niveau::all();
        $matieres= Matiere::with('matiereNiveau')->get();
        $questionMatieres= Question::with('questionMatiere')->get();
        $questionNiveaux= Question::with('questionNiveau')->get();
        $reponses= Reponse::all();

        $questions= Tuto::all();
        return view('question.index', compact('etudiants', 'niveaux','matieres','questionMatieres','questionNiveaux','reponses','questions'));
    }

    public function store(Request $request)
    {


        // $etudiants= User::where('status', 0)->get();
        // $niveaux= Niveau::all();
        // $matieres= Matiere::with('matiereNiveau')->get();

        $data = $request->all();
        $newQuestion= Question::create([
            'contenu'=>$data['contenu'],
            'user_id'=>$data['user'],
            'matiere_id'=>$data['matiere'],
            'niveau_id'=>$data['niveau'],
        ]);


        for($i=0; $i< count($data['contenuReponse']); $i++){

            // echo $i;
            $reponse = new Reponse();

          $reponse->question_id = $newQuestion->id;
          $reponse->status= $data['status'][$i];
          $reponse->contenu = $data['contenuReponse'][$i];
    //   dd($operation);
          $reponse->save();
        }



        return redirect()->back()->with('success','Question ajouter avec succes');
    }

    public function destroy($id)
    {
        Question::find($id)->delete();

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {

        $questions= Question::find($id);
        $questions->contenu= $request->contenu;
        $questions->niveau_id= $request->niveau;
        $questions->matiere_id= $request->matiere;

        $questions->save();


        return redirect()->back()->with('success', 'Modification Effectuer Avec Succes');
    }


}
