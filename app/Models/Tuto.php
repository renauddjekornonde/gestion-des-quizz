<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tuto extends Model
{
    use HasFactory;
    protected $fillable = [
        'contenu',
        'titre',
        'description',
        'image',
        'matiere_id',
        'niveau_id',
        'created_at',
        'updated_at'
    ];
    public function tutoMatiere(){
        return $this->belongsTo(Matiere::class, 'matiere_id');
    }
    public function tutoNiveau(){
        return $this->belongsTo(Niveau::class, 'niveau_id');
    }
    public function tutoUser(){
        return $this->belongsTo(User::class);
    }
}
