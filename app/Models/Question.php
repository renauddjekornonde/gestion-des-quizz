<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $fillable = [
        'contenu',
        'user_id',
        'niveau_id',
        'matiere_id',
        'created_at',
        'updated_at'
    ];
    public function questionUser(){
        return $this->belongsTo(User::class);
    }
    public function questionMatiere(){
        return $this->belongsTo(Matiere::class, 'matiere_id');
    }
    public function questionNiveau(){
        return $this->belongsTo(Niveau::class, 'niveau_id');
    }
    public function questionReponse(){
        return $this->hasMany(Reponse::class);
    }
}
