<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Etudiant extends Model
{
    use HasFactory;
     protected $fillable = [
        'name',
        'email',
        'niveau',
        'avatar',
        'sexe',
        'password',
        'created_at',
        'updated_at'
    ];

    public function etudiantMatiere(){
        return $this->belongsToMany(Matiere::class);
    }
}
