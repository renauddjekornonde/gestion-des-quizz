<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MescOurs extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'tuto_id',
        'created_at',
        'updated_at'
    ];
    public function MesCoursTuto(){
        return $this->belongsTo(Tuto::class, 'tuto_id');
    }
}
