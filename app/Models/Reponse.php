<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{
    use HasFactory;
    protected $fillable = [
        'contenu',
        'question_id',
        'created_at',
        'updated_at'
    ];
    public function reponseQuestion(){
        return $this->belongsTo(Question::class);
    }
}
