<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matiere extends Model
{
    use HasFactory;
    protected $fillable = [
        'contenu',
        'niveau_id',
        'created_at',
        'updated_at'
    ];
    public function matiereQuestion(){
        return $this->hasMany(Question::class);
    }
    public function matiereTuto(){
        return $this->hasMany(Tuto::class);
    }

    public function matiereEtudiant(){
        return $this->belongsToMany(Etudiant::class);
    }

    public function matiereNiveau(){
        return $this->belongsTo(Niveau::class, 'niveau_id');
    }
}
