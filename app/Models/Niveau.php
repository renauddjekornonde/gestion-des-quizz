<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Niveau extends Model
{
    use HasFactory;
    protected $fillable = [
        'nom',
        'created_at',
        'updated_at'
    ];
    public function niveauMatiere(){
        return $this->hasMany(Matiere::class);
    }
    public function niveauEtudiant(){
        return $this->hasMany(Etudiant::class);
    }
}
