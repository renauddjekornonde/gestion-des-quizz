<?php

use App\Http\Controllers\EtudiantController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfesseurController;
use App\Http\Controllers\TutoController;
use App\Http\Controllers\MatiereController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\ReponseController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\BotManController;
use App\Http\Controllers\MescOursController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/admin', [HomeController::class, 'index'])->middleware('auth')->name('home');
Route::get('/', [HomeController::class, 'accueil'])->name('accueil');
Route::get('/professeur', [ProfesseurController::class, 'indexProf'])->name('homeProfesseur');
Route::get('/tuto', [TutoController::class, 'index'])->middleware('auth')->name('tuto');
Route::get('/matiere', [MatiereController::class, 'index'])->middleware('auth')->name('matiere');
Route::get('/question', [QuestionController::class, 'index'])->middleware('auth')->name('question');
Route::get('/service', [HomeController::class, 'service'])->name('service');

Route::get('/listeEtudiant', [EtudiantController::class, 'index'])->middleware('auth')->name('listeEtudiant');
Route::get('/listeProf', [ProfesseurController::class, 'index'])->name('listeProf');
Route::get('/quizzEtudiant/{id}', [HomeController::class, 'indexquizz'])->middleware('auth')->name('quizzEtudiant');

Route::match(['get', 'post'], '/storeProfesseur', [ProfesseurController::class, 'store'])->name('storeProfesseur');
Route::match(['get', 'post'], '/storeTuto', [TutoController::class, 'store'])->middleware('auth')->name('storeTuto');
Route::match(['get', 'post'], '/storeMatiere', [MatiereController::class, 'store'])->middleware('auth')->name('storeMatiere');
Route::match(['get', 'post'], '/storeQuestion', [QuestionController::class, 'store'])->middleware('auth')->name('storeQuestion');
Route::match(['get', 'post'], '/storeEtudiant', [EtudiantController::class, 'store'])->middleware('auth')->name('storeEtudiant');
Route::match(['get', 'post'], '/storeReponse', [ReponseController::class, 'store'])->middleware('auth')->name('storeReponse');

Route::patch('/updateEtudiant/{id}', [EtudiantController::class, 'update'])->middleware('auth')->name('updateEtudiant');
Route::patch('/updateMatiere/{id}', [MatiereController::class, 'update'])->middleware('auth')->name('updateMatiere');
Route::patch('/updateTuto/{id}', [TutoController::class, 'update'])->middleware('auth')->name('updateTuto');
Route::patch('/updateReponse/{id}', [ReponseController::class, 'update'])->middleware('auth')->name('updateReponse');
Route::patch('/updateQuestion/{id}', [QuestionController::class, 'update'])->middleware('auth')->name('updateQuestion');
Route::patch('/setStatut/{id}', [EtudiantController::class, 'setStatut'])->middleware('auth')->name('setStatut');

Route::get('/edit_profil', [EtudiantController::class, 'edit_profil'])->middleware('auth')->name('edit_profil');
Route::patch('/update_edit_profil/{id}', [EtudiantController::class, 'update_edit_profil'])->middleware('auth')->name('update_edit_profil');


Route::get('/userChecker', [UsersController::class, 'userChecker'])->name('userChecker');
Route::get('/logout', [UsersController::class, 'logout'])->name('ap_logout');


Route::delete('/deleteMatiere/{id}', [MatiereController::class, 'destroy'])->middleware('auth')->name('destroyMatiere');
Route::delete('/deleteTuto/{id}', [TutoController::class, 'destroy'])->middleware('auth')->name('destroyTuto');
Route::delete('/deleteReponse/{id}', [ReponseController::class, 'destroy'])->middleware('auth')->name('destroyReponse');
Route::delete('/deleteQuestion/{id}', [QuestionController::class, 'destroy'])->middleware('auth')->name('destroyQuestion');

Route::match(['get', 'post'], 'botman', [BotManController::class, 'handle']);

Route::post('/favori', [MescOursController::class, 'storeMesCours'])->middleware('auth')->name('favori');
Route::get('/mescours', [MescOursController::class, 'index'])->middleware('auth')->name('mescours');
